import express from "express";
import cors from "cors";
import dotenv from "dotenv";
import helmet from "helmet";
import cookieParser from "cookie-parser";
import { verifyToken, verifyAdmin } from "./middleware";
const usersDataRoute = require("./src/Api/usersDataRoute");
const loginRoute = require("./src/Api/loginRoute");
const logoutRoute = require("./src/Api/logoutRoute");
const tribesDataRoute = require("./src/Api/tribesDataRoute");
const squadsDataRoute = require("./src/Api/squadsDataRoute");
const productsDataRoute = require("./src/Api/productsDataRoute");
const productCountRoute = require("./src/Api/productCountRoute");
const productNewRoute = require("./src/Api/productsNewCountRoute");
const productClosedRoute = require("./src/Api/productsClosedCountRoute");
const productInProgressRoute = require("./src/Api/productsInProgressCountRoute");
const singleProductRoute = require("./src/Api/singleProductRoute");
const singleSquadRoute = require("./src/Api/singleSquadRoute");
const singleTribeRoute = require("./src/Api/singleTribeRoute");
const tribeCountRoute = require("./src/Api/tribeCountRoute");
const userCountRoute = require("./src/Api/userCountRoute");
const squadCountRoute = require("./src/Api/squadCountRoute");
const squadCountByTribeRoute = require("./src/Api/squadCountByTribeRoute");
const createUserRoute = require("./src/Api/createUserRoute");
const rolesDataRoute = require("./src/Api/rolesDataRoute");
const createTribeRoute = require("./src/Api/createTribeRoute");
const createSquadRoute = require("./src/Api/createSquadRoute");
const createProductRoute = require("./src/Api/createProductRoute");
const deleteTribeRoute = require("./src/Api/deleteTribeRoute");
const deleteSquadRoute = require("./src/Api/deleteSquadRoute");
const deleteProductRoute = require("./src/Api/deleteProductRoute");
const deleteUserRoute = require("./src/Api/deleteUserRoute");
const productStatusRoute = require("./src/Api/productStatusRoute");
const updateTribeRoute = require("./src/Api/updateTribeRoute");
const singleTribeByIdRoute = require("./src/Api/singleTribeByIdRoute");
const singleSquadByIdRoute = require("./src/Api/singleSquadByIdRoute");
const roleByIdRoute = require("./src/Api/roleByIdRoute");
const squadByIdRoute = require("./src/Api/squadByIdRoute");
const getUserFavoritesRoute = require("./src/Api/getUserFavoritesRoute");
const addUserFavoritesRoute = require("./src/Api/addUserFavoritesRoute");
const deleteUserFavoritesRoute = require("./src/Api/deleteUserFavoritesRoute");
const searchRoute = require("./src/Api/searchRoute");

dotenv.config();

const app = express();
const PORT: number = process.env.PORT ? parseInt(process.env.PORT) : 4000;

app.use(helmet());

// Middleware til at parse cookies, JSON bodies, og URL-encoded bodies
app.use(cookieParser());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// CORS middleware
app.use(
  cors({
    origin: ["http://localhost:3000", "https://boredk-spotlight.netlify.app"],
    credentials: true,
  })
);

// Betjener statiske filer fra "build/"-mappen
app.use(express.static("build"));

// Routes
app.use("/api/login", loginRoute);

// Token beskyttede routes
app.use("/api/logout", verifyToken, logoutRoute);
app.use("/api/tribesData", tribesDataRoute);
app.use("/api/squadsData", squadsDataRoute);
app.use("/api/productsData", productsDataRoute);
app.use("/api/usersData", usersDataRoute);
app.use("/api/rolesData", rolesDataRoute);
app.use("/api/products/productCount", productCountRoute);
app.use("/api/products/productsInProgressCount", productInProgressRoute);
app.use("/api/products/productsNewCount", productNewRoute);
app.use("/api/products/productsClosedCount", productClosedRoute);
app.use("/api/tribes/tribeCount", tribeCountRoute);
app.use("/api/users/userCount", userCountRoute);
app.use("/api/squads/squadCount", squadCountRoute);
app.use("/api/products", singleProductRoute);
app.use("/api/squads", singleSquadRoute);
app.use("/api/tribes", singleTribeRoute);
app.use("/api/squadCountByTribe", squadCountByTribeRoute);
app.use("/api/productStatus", productStatusRoute);
app.use("/api/roleById", roleByIdRoute);
app.use("/api/squadById", squadByIdRoute);
app.use("/api/user", getUserFavoritesRoute);
app.use("/api/favorites", addUserFavoritesRoute);
app.use("/api/favorites", deleteUserFavoritesRoute);
app.use("/api/search", searchRoute);

// Admin beskyttede routes
app.use("/api/createUser", createUserRoute);
app.use("/api/createTribe", createTribeRoute);
app.use("/api/createSquad", createSquadRoute);
app.use("/api/createProduct", createProductRoute);
app.use("/api/deleteTribe", deleteTribeRoute);
app.use("/api/deleteSquad", deleteSquadRoute);
app.use("/api/deleteProduct", deleteProductRoute);
app.use("/api/deleteUser", deleteUserRoute);
app.use("/api/updateTribe", updateTribeRoute);
app.use("/api/singleTribeByIdRoute", singleTribeByIdRoute);
app.use("/api/singleSquadByIdRoute", singleSquadByIdRoute);

app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`);
});
