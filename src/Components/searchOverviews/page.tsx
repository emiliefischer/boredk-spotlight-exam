import React, { useState, useEffect } from "react";

interface SearchFieldProps {
  onSearch: (searchTerm: string) => void;
  placeholder?: string; // Allow custom placeholder text
}

const SearchFieldOverviews: React.FC<SearchFieldProps> = ({
  onSearch,
  placeholder = "Search...",
}) => {
  const [searchTerm, setSearchTerm] = useState("");

  useEffect(() => {
    const delayDebounceFn = setTimeout(() => {
      onSearch(searchTerm);
    }, 300); // debounce delay

    return () => clearTimeout(delayDebounceFn);
  }, [searchTerm, onSearch]);

  return (
    <div className="relative">
      <input
        type="text"
        value={searchTerm}
        onChange={(e) => setSearchTerm(e.target.value)}
        placeholder={placeholder}
        className="w-full px-5 py-2 text-sm border-none rounded-full shadow-md search-field dark:bg-dbSearchDark dark:placeholder-dbWhiteDark dark:text-dbWhite focus:ring-dbGrey"
      />
    </div>
  );
};

export default SearchFieldOverviews;
