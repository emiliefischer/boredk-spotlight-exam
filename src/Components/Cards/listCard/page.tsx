import React, { useState } from "react";
import { FaRegTrashAlt, FaRegEdit } from "react-icons/fa";
import SecondaryButton from "../../Buttons/secondaryBtn/page";
import PrimaryButton from "../../Buttons/primaryBtn/page";
import useDeleteUser from "../../../Hooks/useDeleteUser";
import useDeleteTribe from "../../../Hooks/useDeleteTribe";
import useDeleteSquad from "../../../Hooks/useDeleteSquad";
import useDeleteProduct from "../../../Hooks/useDeleteProduct";
import { useNavigate } from "react-router-dom";

type ListItem = {
  id: string;
  name: string;
  description: string;
  total?: string;
};

type ListProps = {
  title: string;
  th1: string;
  th2: string;
  th3: string;
  th4?: string;
  adminCard: string;
  data: ListItem[];
  secondaryBtnContent: string;
  secondaryBtnLink: string;
  primaryBtnContent: string;
  primaryBtnLink: string;
};

const List: React.FC<ListProps> = ({
  title,
  th1,
  th2,
  th3,
  th4,
  adminCard,
  data,
  secondaryBtnContent,
  secondaryBtnLink,
  primaryBtnContent,
  primaryBtnLink,
}) => {
  const [visibleItems, setVisibleItems] = useState(10);
  const [showConfirmation, setShowConfirmation] = useState(false);
  const [selectedItemId, setSelectedItemId] = useState<string | null>(null);

  const navigate = useNavigate();

  const {
    deleteTribe,
    loading: loadingTribe,
    error: errorTribe,
  } = useDeleteTribe();
  const {
    deleteSquad,
    loading: loadingSquad,
    error: errorSquad,
  } = useDeleteSquad();
  const {
    deleteProduct,
    loading: loadingProduct,
    error: errorProduct,
  } = useDeleteProduct();
  const {
    deleteUser,
    loading: loadingUser,
    error: errorUser,
  } = useDeleteUser();

  const showMoreItems = () => {
    setVisibleItems((prev) => prev + 10);
  };

  const handleEditClick = (itemId: string) => {
    navigate(`/update-tribe/${itemId}`);
  };

  const handleDeleteClick = (itemId: string) => {
    setSelectedItemId(itemId);
    setShowConfirmation(true);
  };

  const confirmDelete = async () => {
    if (selectedItemId) {
      if (adminCard === "Tribes") {
        await deleteTribe(selectedItemId);
      } else if (adminCard === "Squads") {
        await deleteSquad(selectedItemId);
      } else if (adminCard === "Products") {
        await deleteProduct(selectedItemId);
      } else if (adminCard === "Users") {
        await deleteUser(selectedItemId);
      }
      setShowConfirmation(false);
      setSelectedItemId(null);
    }
  };

  const cancelDelete = () => {
    setShowConfirmation(false);
    setSelectedItemId(null);
  };

  return (
    <section className="px-4">
      <h2 className="pb-8 text-h2 dark:text-dbWhite">
        List of all {title} in Danske Bank
      </h2>
      <div className="relative overflow-x-auto">
        <table className="w-full text-sm text-left text-gray-500 rtl:text-right dark:text-gray-400">
          <thead className="text-xs text-dbBlue dark:bg-dbCardBackgroundColorDark dark:text-dbWhite">
            <tr>
              <th scope="col" className="px-6 py-3 text-h5">
                {th1}
              </th>
              <th scope="col" className="px-6 py-3 text-h5">
                {th2}
              </th>
              <th scope="col" className="px-6 py-3 text-h5">
                {th3}
              </th>
              {th4 && (
                <th scope="col" className="px-6 py-3 text-h5">
                  {th4}
                </th>
              )}
            </tr>
          </thead>
          <tbody>
            {data.slice(0, visibleItems).map((item) => (
              <tr key={item.id}>
                <th
                  scope="row"
                  className="px-6 py-4 whitespace-nowrap dark:text-dbWhite"
                >
                  {item.id}
                </th>
                <td className="px-6 py-4">{item.name}</td>
                <td className="px-6 py-4">{item.description}</td>
                {th4 && <td className="px-6 py-4">{item.total}</td>}
                <td className="flex gap-1 px-6 py-4">
                  <button
                    onClick={() => handleEditClick(item.id)}
                    className="font-medium text-blue-600 dark:text-blue-500 hover:underline"
                  >
                    <FaRegEdit />
                  </button>
                  <button
                    onClick={() => handleDeleteClick(item.id)}
                    className="font-medium text-blue-600 dark:text-blue-500 hover:underline"
                  >
                    <FaRegTrashAlt />
                  </button>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
        {visibleItems < data.length && (
          <div className="py-4">
            <SecondaryButton
              content="Show more"
              linkTo="#"
              onClick={showMoreItems}
            />
          </div>
        )}
        <div className="border-b-[1px] border-dbGray"></div>
        <div className="flex gap-6 py-4">
          <SecondaryButton
            content={secondaryBtnContent}
            linkTo={secondaryBtnLink}
          />
          <PrimaryButton content={primaryBtnContent} linkTo={primaryBtnLink} />
        </div>
      </div>

      {showConfirmation && (
        <div className="fixed inset-0 flex items-center justify-center bg-gray-600 bg-opacity-50">
          <div className="p-6 bg-white rounded-lg shadow-lg">
            <p>
              Are you sure you want to delete this {adminCard.slice(0, -1)}?
            </p>
            <div className="flex justify-end gap-4 mt-4">
              <button
                onClick={confirmDelete}
                className="px-4 py-2 text-white bg-red-600 rounded"
              >
                Yes
              </button>
              <button
                onClick={cancelDelete}
                className="px-4 py-2 bg-gray-300 rounded"
              >
                No
              </button>
            </div>
          </div>
        </div>
      )}

      {(loadingTribe || loadingSquad || loadingProduct || loadingUser) && (
        <p>Loading...</p>
      )}
      {(errorTribe || errorSquad || errorProduct || errorUser) && (
        <p>Error: {errorTribe || errorSquad || errorProduct || errorUser}</p>
      )}
    </section>
  );
};

export default List;
