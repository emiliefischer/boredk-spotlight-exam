import React, { useEffect, useState } from "react";
import projectImg from "../../../Assets/images/projectImg.png";
import { FaHeart } from "react-icons/fa";
import { FaCircleArrowUp } from "react-icons/fa6";
import { Product } from "../../../Types/productInterface";
import { getStatusColorClass } from "../../../Utils/statusUtils";
import { Link } from "react-router-dom";
import useUserFavorites from "../../../Hooks/useUserFavorites";
import useFetchCookieUser from "../../../Hooks/useFetchCookieUser";

interface ProductCardProps {
  product: Product;
}

const ProductCard: React.FC<ProductCardProps> = ({ product }) => {
  const statusColorClass = getStatusColorClass(product.status_name);
  const { addFavorite, removeFavorite, error, loading } = useUserFavorites();
  const { user } = useFetchCookieUser();
  const [liked, setLiked] = useState(false);

  useEffect(() => {
    const likedProducts = JSON.parse(
      localStorage.getItem("likedProducts") || "[]"
    );
    setLiked(likedProducts.includes(product.product_id));
  }, [product.product_id]);

  const handleLikeClick = async (event: React.MouseEvent) => {
    event.stopPropagation();
    event.preventDefault();

    if (!user) {
      console.log("User not logged in");
      return;
    }

    try {
      if (liked) {
        await removeFavorite(user.user_id, product.product_id);
        setLiked(false);

        const likedProducts = JSON.parse(
          localStorage.getItem("likedProducts") || "[]"
        ).filter((id: string) => id !== product.product_id);
        localStorage.setItem("likedProducts", JSON.stringify(likedProducts));
      } else {
        await addFavorite(user.user_id, product.product_id);
        setLiked(true);

        const likedProducts = JSON.parse(
          localStorage.getItem("likedProducts") || "[]"
        );
        likedProducts.push(product.product_id);
        localStorage.setItem("likedProducts", JSON.stringify(likedProducts));
      }
    } catch (error) {
      console.error("Error toggling product favorite:", error);
    }
  };

  return (
    <Link
      to={`/products/${encodeURIComponent(
        product.product_title.toLowerCase().replace(/\s+/g, "-")
      )}`}
      className="block"
    >
      <div className="flex flex-col h-full p-4 space-y-2 rounded-sm bg-dbSand dark:bg-dbNavBackgroundColorDark">
        <div className="flex items-center justify-between">
          <div className={`px-4 py-0 rounded-sm ${statusColorClass}`}>
            <p className="text-sm text-dbWhite dark:text-dbCardBackgroundColorDark">
              {product.status_name}
            </p>
          </div>
          <FaCircleArrowUp
            className="w-5 h-6 mr-1 rotate-45 fill-dbBrightBlue dark:fill-dbBrightBlueDark"
            data-testid="status-icon"
          />
        </div>
        {product.product_image ? (
          <img
            className="h-[137px] object-cover"
            src={product.product_image}
            alt={product.product_title}
          />
        ) : (
          <img src={projectImg} alt="Default Image" />
        )}
        <h3 className="font-semibold text-h4 text-dbBlue dark:text-dbWhiteDark">
          {product.product_title}
        </h3>
        <h4
          className="font-semibold text-h5 text-dbBlue dark:text-dbWhiteDark"
          data-testid="product-owner"
        >
          <strong>PO:</strong> {product.product_owner_name}
        </h4>
        <p className="text-md text-dbBlue dark:text-dbWhiteDark">
          {product.product_description}
        </p>
        <div className="flex items-end justify-end flex-grow">
          <FaHeart
            className={`w-6 h-6 cursor-pointer ${
              liked ? "text-dbBrightBlue" : "text-dbLightBlue"
            } hover:text-dbBrightBlue transition-colors duration-200`}
            onClick={handleLikeClick}
          />
        </div>
      </div>
    </Link>
  );
};

export default ProductCard;
