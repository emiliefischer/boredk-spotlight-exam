import React from "react";
import { render, screen, fireEvent } from "@testing-library/react";
import SecondaryButton from "./page";

describe("SecondaryButton component", () => {
  test("renders with correct content and link", () => {
    const content = "Click me";
    const linkTo = "/example";
    render(<SecondaryButton content={content} linkTo={linkTo} />);

    const buttonElement = screen.getByText(content);
    expect(buttonElement).toBeInTheDocument();
    expect(buttonElement.tagName).toBe("A");
    expect(buttonElement).toHaveAttribute("href", linkTo);
  });

  test("calls onClick callback when clicked", () => {
    const onClick = jest.fn();
    render(<SecondaryButton content="Click me" linkTo="/" onClick={onClick} />);

    const buttonElement = screen.getByText("Click me");
    fireEvent.click(buttonElement);

    expect(onClick).toHaveBeenCalled();
  });
});
