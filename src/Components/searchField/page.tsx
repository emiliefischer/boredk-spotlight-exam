import React, { useState, useEffect } from "react";
import axios from "axios";
import { Link } from "react-router-dom";

const SearchField: React.FC = () => {
  const [searchTerm, setSearchTerm] = useState("");
  const [results, setResults] = useState<any[]>([]);
  const [isSearching, setIsSearching] = useState(false);

  useEffect(() => {
    if (searchTerm.trim() === "") {
      setResults([]);
      return;
    }

    const delayDebounce = setTimeout(() => {
      const fetchData = async () => {
        try {
          setIsSearching(true);
          const response = await axios.get(
            `http://localhost:4000/api/search?q=${searchTerm}`
          );
          setResults(response.data);
          setIsSearching(false);
        } catch (error) {
          console.error("Error searching:", error);
          setIsSearching(false);
        }
      };

      fetchData();
    }, 300); // debounce forsinkelse

    return () => clearTimeout(delayDebounce);
  }, [searchTerm]);

  const getResultLink = (result: any) => {
    switch (result.type) {
      case "product":
        return `/products/${encodeURIComponent(
          result.name.toLowerCase().replace(/\s+/g, "-")
        )}`;
      case "tribe":
        return `/tribes/${encodeURIComponent(
          result.name.toLowerCase().replace(/\s+/g, "-")
        )}`;
      case "squad":
        return `/squads/${encodeURIComponent(
          result.name.toLowerCase().replace(/\s+/g, "-")
        )}`;
      default:
        return "#";
    }
  };

  return (
    <div className="relative">
      <input
        type="text"
        value={searchTerm}
        onChange={(e) => setSearchTerm(e.target.value)}
        placeholder="Search for products, tribes, squads..."
        className="w-full px-5 py-2 text-sm border-none rounded-full shadow-md search-field bg-dbSand dark:bg-dbSearchDark dark:placeholder-dbWhiteDark dark:text-dbWhite focus:ring-dbGrey"
      />
      {isSearching && <p>Searching...</p>}
      {results.length > 0 && (
        <div className="absolute left-0 w-[150%] bg-dbSand dark:bg-dbSearchDark dark:text-dbWhiteDark shadow-md mt-2 rounded-lg z-10">
          {results.map((result) => (
            <Link
              key={result.id}
              to={getResultLink(result)}
              className="block p-2 hover:bg-gray-200 dark:hover:bg-dbBoxShadeOneDark"
            >
              <p className="text-sm">
                {result.name} ({result.type})
              </p>
            </Link>
          ))}
        </div>
      )}
    </div>
  );
};

export default SearchField;
