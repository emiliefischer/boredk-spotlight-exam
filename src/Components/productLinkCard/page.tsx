import React from "react";
import { Button } from "flowbite-react";

type ProductLinkProps = {
  image: string;
  link: string;
};

const ProductLinkCard = ({ image, link }: ProductLinkProps) => {
  return (
    <div className="flex flex-col items-center text-dbBlue dark:text-dbWhite">
      <img src={image} alt="link_picture" className="w-12 mx-auto my-4" />
      <button className="text-dbBrightBlue dark:text-dbBrightBlueDark">
        Link
      </button>
    </div>
  );
};

export default ProductLinkCard;
