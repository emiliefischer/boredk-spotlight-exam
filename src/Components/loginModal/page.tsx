import React, { useState } from "react";
import { CgClose } from "react-icons/cg";
import { useNavigate } from "react-router-dom";
import axios from "axios";

const LoginModalPopup = () => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [error, setError] = useState<string | null>(null);
  const navigate = useNavigate();

  const sanitizeInput = (input: string) => {
    // Remove leading and trailing whitespaces
    return input.trim();
  };

  const validateForm = () => {
    const sanitizedEmail = sanitizeInput(email);
    const sanitizedPassword = sanitizeInput(password);
    if (!sanitizedEmail.includes("@")) {
      setError("Email must include an @ symbol");
      return false;
    }
    if (sanitizedEmail.length !== 18) {
      setError("Email is not the right lenght");
      return false;
    }
    if (sanitizedPassword.trim() === "") {
      setError("Password cannot be empty");
      return false;
    }
    return true;
  };

  const handleLogin = async (e: React.FormEvent) => {
    e.preventDefault();
    setError("");

    if (!validateForm()) {
      return;
    }

    try {
      const sanitizedEmail = sanitizeInput(email);
      const sanitizedPassword = sanitizeInput(password);

      const response = await axios.post(
        "http://localhost:4000/api/login",
        {
          user_email: sanitizedEmail,
          user_password: sanitizedPassword,
        },
        { withCredentials: true }
      );

      // Store the token in localStorage
      localStorage.setItem("token", response.data.token);
      localStorage.setItem("user", JSON.stringify(response.data.user));

      // Store the user roles in localStorage
      localStorage.setItem(
        "userRoles",
        JSON.stringify({
          isSuperiorAdmin: response.data.isSuperiorAdmin,
          isSquadAdmin: response.data.isSquadAdmin,
          isTribeAdmin: response.data.isTribeAdmin,
        })
      );

      // Trigger a storage event to notify other tabs
      window.dispatchEvent(new Event("storage"));

      // Close the login module after successful login
      closeLoginModule();

      // Check user role and redirect accordingly
      if (
        response.data.isSuperiorAdmin ||
        response.data.isSquadAdmin ||
        response.data.isTribeAdmin
      ) {
        navigate("/admindashboard");
      } else {
        navigate("/userprofile");
      }
    } catch (err) {
      console.error("Error during login:", err);
      setError("Invalid email or password");
    }
  };

  const closeLoginModule = () => {
    const loginModule = document.getElementById("login_module");
    loginModule?.classList.add("hidden");
    loginModule?.classList.remove("grid");
    setEmail("");
    setPassword("");
    setError(null);
  };

  return (
    <div
      id="login_module"
      className="fixed top-0 left-0 z-50 items-center justify-center hidden w-full h-screen bg-slate-900/50 backdrop-blur-sm"
    >
      <div className="p-6 md:p-10 flex flex-col items-center justify-center w-fill md:w-[600px] m-4 bg-dbBlue rounded-lg dark:bg-dbNavBackgroundColorDark">
        <button
          type="button"
          onClick={closeLoginModule}
          className="cursor-pointer ml-[100%]"
        >
          <CgClose className="text-dbWhiteDark" />
        </button>
        <p className="mb-6 text-h4 text-dbWhiteDark">
          Login to BOREDK Spotlight Platform
        </p>
        <form className="w-full" onSubmit={handleLogin}>
          <div className="flex flex-col gap-4">
            <div className="flex flex-col w-full gap-4">
              <div className="flex flex-col w-full gap-2">
                <label
                  htmlFor="email"
                  className="text-sm w-fit text-dbWhiteDark"
                >
                  Email
                </label>
                <input
                  className="input_field dark:bg-dbNavBackgroundColorDark dark:text-dbWhiteDark"
                  type="text"
                  id="email"
                  value={email}
                  onChange={(e) => setEmail(e.target.value)}
                  placeholder="Email"
                />
              </div>
              <div className="flex flex-col w-full gap-2">
                <label
                  htmlFor="password"
                  className="text-sm text-gray-100 w-fit"
                >
                  Password
                </label>
                <input
                  className="input_field dark:bg-dbNavBackgroundColorDark dark:text-dbWhiteDark"
                  type="password"
                  id="password"
                  value={password}
                  onChange={(e) => setPassword(e.target.value)}
                  placeholder="Password"
                />
              </div>
            </div>
            <div className="my-8">
              {error && <div className="text-red-500">{error}</div>}{" "}
              <button
                className="flex gap-2 px-5 py-2 transition ease-in-out rounded-full shadow-md text-md text-dbSand bg-dbBrightBlue hover:bg-dbBrightBlueHover dark:text-dbCardBackgroundColorDark dark:bg-dbBrightBlueDark dark:bg-db"
                type="submit"
              >
                Log in
              </button>
            </div>
          </div>
        </form>
      </div>
    </div>
  );
};

export default LoginModalPopup;
