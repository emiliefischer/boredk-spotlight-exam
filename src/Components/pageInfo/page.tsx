import React from "react";

type SectionProps = {
  title: string;
  secondTitle: string;
  content: string;
};

const PageInfo = ({ title, secondTitle, content }: SectionProps) => (
  <section className="p-8 page-info">
    <div className="mb-8">
      <h1 className="py-2 font-medium text-h2 dark:text-dbWhiteDark ">
        {title}
      </h1>
      <h1 className="font-medium text-h1 dark:text-dbWhiteDark ">
        {secondTitle}
      </h1>
    </div>
    <p className="mt-2 text-base lg:col-span-2 text-dbBlue dark:text-dbWhiteDark max-w-readable">
      {content}
    </p>
  </section>
);

export default PageInfo;
