import React, { useState } from "react";

interface FilterFieldProps {
  options: string[];
  onSelect: (selectedStatus: string) => void;
}

const FilterField: React.FC<FilterFieldProps> = ({ options, onSelect }) => {
  const [selectedStatus, setSelectedStatus] = useState("");

  const handleStatusChange = (e: React.ChangeEvent<HTMLSelectElement>) => {
    const selected = e.target.value;
    setSelectedStatus(selected);
    onSelect(selected); // Send status tilbage til parent komponentet
  };

  return (
    <div className="flex items-center">
      <label htmlFor="statusFilter" className="mr-2 text-md dark:text-dbWhite">
        Filter by Status:
      </label>
      <select
        id="statusFilter"
        value={selectedStatus}
        onChange={handleStatusChange}
        className="px-3 py-2 text-sm border-none rounded-md shadow-md bg-dbSand focus:ring-dbGrey dark:text-dbWhite dark:bg-dbSearchDark"
      >
        <option value="">All</option>
        {options.map((option, index) => (
          <option key={index} value={option.toLowerCase()}>
            {option}
          </option>
        ))}
      </select>
    </div>
  );
};

export default FilterField;
