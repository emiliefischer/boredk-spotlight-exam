export const getStatusColorClass = (statusName: string | undefined): string => {
  if (typeof statusName !== "string") {
    return "bg-dbGrey dark:bg-dbSearchDark"; // Default class for undefined or non-string status
  }

  switch (statusName.toLowerCase()) {
    case "new":
      return "bg-dbLabelGreen dark:bg-dbLabelGreenDark";
    case "in progress":
      return "bg-dbLabelPink dark:bg-dbLabelPinkDark";
    case "closed":
      return "bg-dbLabelOrange dark:bg-dbLabelOrangeDark";
    case "on hold":
      return "bg-dbBrightBlue dark:bg-dbBrightBlueDark";
    case "cancelled":
      return "bg-dbBlue dark:bg-dbLightBlueDark";
    default:
      return "bg-dbGrey dark:bg-dbSearchDark";
  }
};
