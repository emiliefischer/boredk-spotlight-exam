export interface Product {
  product_id: string;
  product_title: string;
  product_description: string;
  product_created_at: string;
  product_status_fk: string;
  status_name: string;
  product_owner_name: string;
  product_image: string | null;
}
