import React from "react";
import SearchField from "../../Components/searchField/page";
const NavBarNotLogged = () => {
  const openLoginModule = () => {
    const loginModule = document.getElementById("login_module");
    loginModule?.classList.remove("hidden");
    loginModule?.classList.add("grid");
  };

  return (
    <div className="sticky top-0 z-10 flex items-center justify-between h-16 px-4 py-4 bg-white border-b border-gray-200 dark:border-none dark:bg-dbCardBackgroundColorDark">
      <div className="flex font-medium">
        <a href="/" className="text-dbBlue dark:text-dbWhiteDark">
          BOREDK Spotlight
        </a>
      </div>
      <div className="flex items-center">
        <button
          type="button"
          onClick={openLoginModule}
          className="flex items-center ml-4 btn-login login_btn"
        >
          <svg
            className="w-6 h-6 text-dbBlue"
            aria-hidden="true"
            xmlns="http://www.w3.org/2000/svg"
            fill="none"
            viewBox="0 0 24 24"
          >
            <path
              stroke="currentColor"
              strokeWidth="1"
              d="M7 17v1a1 1 0 0 0 1 1h8a1 1 0 0 0 1-1v-1a3 3 0 0 0-3-3h-4a3 3 0 0 0-3 3Zm8-9a3 3 0 1 1-6 0 3 3 0 0 1 6 0Z"
            />
          </svg>
          <p>Login</p>
        </button>
      </div>
    </div>
  );
};

export default NavBarNotLogged;
