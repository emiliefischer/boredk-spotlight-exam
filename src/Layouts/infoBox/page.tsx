import React from "react";
import InfoLinkBoxCard from "../../Components/Cards/infoLinkBoxCard/page";

const InfoBoxSection = () => {
  return (
    <section className="px-4 my-4">
      <div className="grid gap-4 md:grid-cols-2 lg:grid-cols-3">
        <InfoLinkBoxCard
          bgColorLight="bg-dbGrey"
          bgColorDark="bg-dbCardBackgroundColorDark"
          title="Explore all Tribes: Discover the Heart of Our Community"
          content="Dive into the diverse and vibrant tapestry of tribes that form the backbone of our organization. Explore their unique cultures, values, and missions, and learn how they contribute to our shared vision."
          link="/tribes"
        />
        <InfoLinkBoxCard
          bgColorLight="bg-dbSand"
          bgColorDark="bg-dbNavBackgroundColorDark"
          title="Browse all Squads: Unveil the Teams Shaping Our Future"
          content="Get acquainted with the dynamic squads that are driving innovation and shaping our future. From cross-functional teams to specialized units, discover how these squads collaborate and deliver impactful outcomes."
          link="/squads"
        />
        <InfoLinkBoxCard
          bgColorLight="bg-dbSand"
          bgColorDark="bg-dbNavBackgroundColorDark"
          title="Discover all Products: Dive into Our Innovation Portfolio"
          content="Explore our extensive innovation portfolio and delve into the exciting projects that are pushing boundaries and driving progress across the organization. From initiatives to improvements, see how we're making an impact."
          link="/products"
        />
      </div>
    </section>
  );
};

export default InfoBoxSection;
