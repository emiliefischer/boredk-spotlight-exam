import { useState, useEffect } from "react";
import axios from "axios";
import { User } from "../Types/userInterface";

const useFetchCurrentUser = (userId: string | null) => {
  const [user, setUser] = useState<User | null>(null);
  const [loading, setLoading] = useState<boolean>(true);
  const [error, setError] = useState<string | null>(null);

  useEffect(() => {
    const fetchCurrentUser = async () => {
      try {
        if (userId) {
          const response = await axios.get<User>(
            `https://boredk-spotlight.netlify.app/.netlify/functions/getCurrentUserRoute`,
            {
              withCredentials: true,
              params: { userId },
            }
          );
          setUser(response.data);
        } else {
          setError("No user ID provided");
        }
      } catch (error) {
        console.error("Error fetching user data:", error);
        setError("Error fetching user data");
      } finally {
        setLoading(false);
      }
    };

    fetchCurrentUser();
  }, [userId]);

  return { user, loading, error };
};

export default useFetchCurrentUser;
