import axios from "axios";
import { useEffect, useState } from "react";

const useCountProducts = () => {
  const [productCount, setProductCount] = useState<number | null>(null);
  const [loading, setLoading] = useState<boolean>(true);

  useEffect(() => {
    const fetchProductCount = async () => {
      try {
        const baseUrl =
          process.env.NODE_ENV === "development"
            ? process.env.REACT_APP_API_BASE_URL_LOCAL
            : process.env.REACT_APP_API_BASE_URL_PROD;

        let apiUrl;
        if (process.env.NODE_ENV === "development") {
          apiUrl = `${baseUrl}/products/productCount`;
        } else {
          apiUrl = `${baseUrl}/productCountRoute`;
        }

        const response = await axios.get<{ productCount: number }>(apiUrl);
        setProductCount(response.data.productCount);
        setLoading(false);
      } catch (error) {
        console.error("Error fetching product count:", error);
        setLoading(false);
      }
    };

    fetchProductCount();
  }, []);
  return { productCount, loading };
};

export default useCountProducts;
