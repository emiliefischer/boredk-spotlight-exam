import { useState, useEffect } from "react";
import axios from "axios";
import useFetchCookieUser from "./useFetchCookieUser";
import { Product } from "../Types/productInterface";

const useFetchUserFavorites = () => {
  const { user } = useFetchCookieUser();
  const [favorites, setFavorites] = useState<Product[]>([]);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState<string | null>(null);

  useEffect(() => {
    const fetchFavorites = async () => {
      if (!user) {
        setLoading(false);
        return;
      }
      try {
        const response = await axios.get<{ favoriteProducts: Product[] }>(
          `http://localhost:4000/api/user/${user.user_id}/favorites`
        );
        console.log("Response from backend:", response);
        setFavorites(response.data.favoriteProducts);
      } catch (error) {
        console.error("Error fetching favorites:", error);
        setError("Failed to fetch favorite products.");
      } finally {
        setLoading(false);
      }
    };

    fetchFavorites();
  }, [user]);

  return { favorites, loading, error };
};

export default useFetchUserFavorites;
