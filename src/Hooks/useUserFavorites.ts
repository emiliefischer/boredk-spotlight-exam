import { useState } from "react";
import axios, { AxiosError } from "axios";

const useUserFavorites = () => {
  const [error, setError] = useState<string | null>(null);
  const [loading, setLoading] = useState(false);

  const addFavorite = async (user_id: string, product_id: string) => {
    setLoading(true);
    try {
      const response = await axios.post("http://localhost:4000/api/favorites", {
        user_id,
        product_id,
      });
      console.log("Response:", response.data);
      setError(null);
    } catch (error) {
      handleAxiosError(error);
    } finally {
      setLoading(false);
    }
  };

  const removeFavorite = async (user_id: string, product_id: string) => {
    setLoading(true);
    try {
      const response = await axios.delete(
        "http://localhost:4000/api/favorites",
        {
          data: { user_id, product_id },
        }
      );
      console.log("Response:", response.data);
      setError(null);
    } catch (error) {
      handleAxiosError(error);
    } finally {
      setLoading(false);
    }
  };

  const handleAxiosError = (error: any) => {
    if (axios.isAxiosError(error)) {
      const axiosError = error as AxiosError;
      console.error("Axios Error:", axiosError.message);
      setError("Network error. Please try again later.");
    } else {
      console.error("Error:", error);
      setError("Internal server error. Please try again later.");
    }
  };

  return { addFavorite, removeFavorite, error, loading };
};

export default useUserFavorites;
