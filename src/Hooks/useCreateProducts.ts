import axios, { AxiosError } from "axios";
import { useState } from "react";

interface CreateProductResponse {
  message: string;
}

const useCreateProduct = () => {
  const [loading, setLoading] = useState<boolean>(false);
  const [error, setError] = useState<string | null>(null);
  const [successMessage, setSuccessMessage] = useState<string | null>(null);

  const createProduct = async (formData: FormData) => {
    setLoading(true);
    setError(null);
    setSuccessMessage(null);

    try {
      const response = await axios.post<CreateProductResponse>(
        "http://localhost:4000/api/createProduct", // Make sure this URL is correct
        formData,
        {
          headers: { "Content-Type": "multipart/form-data" },
        }
      );
      setSuccessMessage(response.data.message);
    } catch (error) {
      if (axios.isAxiosError(error)) {
        const axiosError = error as AxiosError;
        if (axiosError.response && axiosError.response.data) {
          const responseData = axiosError.response.data as { error: string };
          setError(responseData.error);
        } else {
          setError("An error occurred while processing your request");
        }
      } else {
        setError("An error occurred while processing your request");
      }
    } finally {
      setLoading(false);
    }
  };

  const resetSuccessMessage = () => {
    setSuccessMessage(null);
  };

  return { createProduct, loading, error, successMessage, resetSuccessMessage };
};

export default useCreateProduct;
