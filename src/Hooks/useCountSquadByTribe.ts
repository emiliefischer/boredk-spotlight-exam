import { useEffect, useState } from "react";
import axios from "axios";

const useCountSquadsByTribe = (tribeId: string) => {
  const [squadCount, setSquadCount] = useState<number | null>(null);
  const [loading, setLoading] = useState<boolean>(true);
  const [error, setError] = useState<string | null>(null);

  useEffect(() => {
    const fetchSquadCount = async () => {
      try {
        const baseUrl =
          process.env.NODE_ENV === "development"
            ? process.env.REACT_APP_API_BASE_URL_LOCAL
            : process.env.REACT_APP_API_BASE_URL_PROD;

        let apiUrl;
        if (process.env.NODE_ENV === "development") {
          apiUrl = `${baseUrl}/squadCountByTribe/${tribeId}`;
        } else {
          apiUrl = `${baseUrl}/squadCountByTribeRoute/${tribeId}`;
        }
        const response = await axios.get<{ squadCount: number }>(apiUrl);
        setSquadCount(response.data.squadCount);
        setLoading(false);
      } catch (error) {
        console.error("Error fetching squad count:", error);
        setError("Error fetching squad count");
        setLoading(false);
      }
    };

    fetchSquadCount();
  }, [tribeId]);

  return { squadCount, loading, error };
};

export default useCountSquadsByTribe;
