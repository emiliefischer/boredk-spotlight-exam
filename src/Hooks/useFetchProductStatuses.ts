import { useState, useEffect } from "react";
import axios from "axios";

interface ProductStatus {
  status_id: string;
  status_name: string;
}

const useFetchProductStatuses = () => {
  const [statuses, setStatuses] = useState<ProductStatus[]>([]);
  const [loading, setLoading] = useState<boolean>(true);
  const [error, setError] = useState<string | null>(null);

  useEffect(() => {
    const fetchStatuses = async () => {
      try {
        const response = await axios.get<ProductStatus[]>(
          "http://localhost:4000/api/productStatus"
        );
        setStatuses(response.data);
      } catch (error) {
        setError("Error fetching product statuses");
      } finally {
        setLoading(false);
      }
    };

    fetchStatuses();
  }, []);

  return { statuses, loading, error };
};

export default useFetchProductStatuses;
