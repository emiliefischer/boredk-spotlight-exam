import React, { useEffect, useState } from "react";
import PageInfo from "../../../Components/pageInfo/page";
import Section from "../../../Layouts/section/page";
import SearchField from "../../../Components/searchField/page";
import PrimaryButton from "../../../Components/Buttons/primaryBtn/page";
import SquadMemberCard from "../../../Components/Cards/squadMemberCard/page";
import FilterField from "../../../Components/filterField/page";
import projectImg from "../../../Assets/images/projectImg.png";
import { FaCircleArrowUp } from "react-icons/fa6";
import useFetchProducts from "../../../Hooks/useFetchProducts";
import ProductCard from "../../../Components/Cards/productCard/page";
import { useParams } from "react-router-dom";
import { Squad } from "../../../Types/squadInterface";
import axios from "axios";

const SingleSquad = () => {
  const {
    products,
    loading: productsLoading,
    error: productsError,
  } = useFetchProducts();

  const { squadName } = useParams<{ squadName: string }>();
  const [squad, setSquad] = useState<Squad | null>(null);

  useEffect(() => {
    const fetchSquad = async () => {
      try {
        const baseUrl =
          process.env.NODE_ENV === "development"
            ? process.env.REACT_APP_API_BASE_URL_LOCAL
            : process.env.REACT_APP_API_BASE_URL_PROD;

        let apiUrl;
        if (process.env.NODE_ENV === "development") {
          apiUrl = `${baseUrl}/squads/${squadName}`;
        } else {
          apiUrl = `${baseUrl}/squadsDataRoute/${squadName}`;
        }

        const response = await axios.get<Squad>(apiUrl);
        setSquad(response.data);
      } catch (error) {
        console.error("Error fetching tribe:", error);
      }
    };
    fetchSquad();
  }, [squadName]);

  if (!squad) {
    return <div>Loading...</div>;
  }

  return (
    <div className=" bg-dbBackgroundColor dark:bg-dbBackgroundColorDark">
      <div className="grid lg:grid-cols-2">
        <PageInfo
          title="About"
          secondTitle={squad.squad_name}
          content={squad.squad_description}
        />
        <div className="flex justify-center my-12">
          <div className="p-4 space-y-2 rounded-sm shadow-2xl w-96 bg-dbWhite dark:bg-dbNavBackgroundColorDark">
            <div className="flex items-center justify-between">
              <div className="px-4 py-0 rounded-sm bg-dbLabelGreen dark:bg-dbLabelGreenDark">
                <p className="text-sm text-dbWhite dark:text-dbBlue">New</p>
              </div>
              <FaCircleArrowUp className="w-5 h-6 mr-1 rotate-45 fill-dbBrightBlue dark:fill-dbBrightBlueDark" />
            </div>
            <img src={projectImg} alt="Logo" />
            <h3 className="font-semibold text-h4 text-dbBlue dark:text-dbWhiteDark">
              Project name
            </h3>
            <h4 className="font-semibold text-h5 text-dbBlue dark:text-dbWhiteDark">
              PO: Name, Lastname
            </h4>
            <p className="text-md text-dbBlue dark:text-dbWhiteDark">
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
              eiusmod tempor incididunt ut labore et dolore magna aliqua.
            </p>
          </div>
        </div>
      </div>
      <section className="lg:-mt-48 bg-dbLightBlue dark:bg-dbCardBackgroundColorDark">
        <div className="px-4 pb-12 pt-44">
          <h2 className="mb-8 font-medium text-center text-h2 text-dbBlue dark:text-dbWhiteDark">
            Squad members
          </h2>
          <div className="grid gap-5 pb-8 sm:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4">
            <SearchField />
          </div>
          <div className="grid gap-5 sm:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4">
            <SquadMemberCard />
            <SquadMemberCard />
            <SquadMemberCard />
            <SquadMemberCard />
            <SquadMemberCard />
            <SquadMemberCard />
            <SquadMemberCard />
            <SquadMemberCard />
            <SquadMemberCard />
            <SquadMemberCard />
            <SquadMemberCard />
            <SquadMemberCard />
          </div>
          <div className="flex justify-center my-8">
            <PrimaryButton content="See all members" linkTo="#" />
          </div>
        </div>
      </section>
      <Section
        bgColorLightMode="bg-dbBackgroundColor"
        bgColorDarkMode="bg-dbBackgroundColorDark"
      >
        <div className="px-4">
          <h2 className="mb-8 font-medium text-center text-h2 text-dbBlue dark:text-dbWhiteDark">
            All projects made by Squad
          </h2>
          <div className="grid gap-5 pb-8 sm:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4">
            <SearchField />
            {/* <FilterField /> */}
          </div>
          <div className="grid gap-5 sm:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4">
            {productsLoading ? (
              <p>Loading...</p>
            ) : productsError ? (
              <p className="text-center text-red-500">{productsError}</p>
            ) : (
              products.map((product) => (
                <ProductCard key={product.product_title} product={product} />
              ))
            )}
          </div>
          <div className="flex justify-center my-8">
            <PrimaryButton content="Show all projects" linkTo="#" />
          </div>
        </div>
      </Section>
      <section className="bg-dbBlue dark:bg-dbCardBackgroundColorDark">
        <div className="px-4 py-12">
          <h2 className="mb-8 font-medium text-center text-h2 text-dbSand ">
            Latest updates
          </h2>
          <div className="grid flex-wrap justify-center grid-cols-1 gap-4 md:grid-cols-2 lg:grid-cols-4">
            {productsLoading ? (
              <p>Loading...</p>
            ) : productsError ? (
              <p className="text-center text-red-500">{productsError}</p>
            ) : (
              products.map((product) => (
                <ProductCard key={product.product_title} product={product} />
              ))
            )}
          </div>
        </div>
      </section>
    </div>
  );
};

export default SingleSquad;
