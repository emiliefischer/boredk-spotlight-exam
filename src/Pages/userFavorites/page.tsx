import React from "react";
import Section from "../../Layouts/section/page";
import PageInfo from "../../Components/pageInfo/page";
import PrimaryButton from "../../Components/Buttons/primaryBtn/page";
import useFetchUserFavorites from "../../Hooks/useFetchUserFavorites";
import ProductCard from "../../Components/Cards/productCard/page";
import { Product } from "../../Types/productInterface";

const UserFavoritesPage: React.FC = () => {
  const {
    favorites,
    loading: favoritesLoading,
    error: favoritesError,
  } = useFetchUserFavorites();

  return (
    <div className="bg-dbBackgroundColor dark:bg-dbBackgroundColorDark">
      <PageInfo
        title="My Favorites"
        secondTitle="Explore Your Liked Products"
        content="Welcome to My Favorites! Here, you can view, manage, and explore all the products you have liked. Whether it's keeping track of your preferred items or revisiting your top choices, this page is your personalized collection of liked products. Enjoy easy access to details, updates, and more, all in one place."
      />
      <Section
        bgColorLightMode="bg-dbBackgroundColor"
        bgColorDarkMode="bg-dbCardBackgroundColorDark"
      >
        <div className="px-8">
          {favoritesLoading ? (
            <p>Loading...</p>
          ) : favoritesError ? (
            <p>{favoritesError}</p>
          ) : favorites.length === 0 ? (
            <p className="text-dbBrightBlue">
              You currently don't have any favorite Products.
            </p>
          ) : (
            <div className="grid gap-5 sm:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4">
              {favorites.map((product: Product) => (
                <ProductCard key={product.product_id} product={product} />
              ))}
            </div>
          )}
          <div className="flex justify-center my-8">
            <PrimaryButton content="See more Products" linkTo="/products" />
          </div>
        </div>
      </Section>
    </div>
  );
};

export default UserFavoritesPage;
