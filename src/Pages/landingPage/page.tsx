import React from "react";
import PageInfo from "../../Components/pageInfo/page";
import StatisticsOverview from "../../Components/statisticsOverview/page";
import ProductCard from "../../Components/Cards/productCard/page";
import Section from "../../Layouts/section/page";
import SquadCard from "../../Components/Cards/squadCard/page";
import PrimaryButton from "../../Components/Buttons/primaryBtn/page";
import useAuth from "../../Hooks/useAuthHook";
import useFetchSquads from "../../Hooks/useFetchSquads";
import useFetchProducts from "../../Hooks/useFetchProducts";
import InfoBoxSection from "../../Layouts/infoBox/page";
import useFetchCookieUser from "../../Hooks/useFetchCookieUser";
import InfoLinkBoxCard from "../../Components/Cards/infoLinkBoxCard/page";

const Home: React.FC = () => {
  const loggedIn = useAuth();

  const { user } = useFetchCookieUser();
  const {
    squads,
    loading: squadsLoading,
    error: squadsError,
  } = useFetchSquads(4);
  const {
    products,
    loading: productsLoading,
    error: productsError,
  } = useFetchProducts(4);

  if (!user) {
    return (
      <div className="bg-dbBackgroundColor dark:bg-dbBackgroundColorDark">
        <div>
          <PageInfo
            title="Welcome to"
            secondTitle="BOREDK Spotlight Platform"
            content="BOREDK Spotlight Platform is Danske Bank's new internal release hub. The platform is designed to be your central source for internal knowledge sharing. Here you can follow what is happening throughout Danske Bank, from product development to business analyses. Our goal is to create an open and informative platform that makes it easy for all employees to stay updated and collaborate across departments. Login to access the platform data."
          />
        </div>
        <div className="h-full pb-20 dark:bg-dbBackgroundColorDark text-dbWhite dark:text-dbBackgroundColorDark">
          <section className="px-4 my-4">
            <div className="grid gap-4 md:grid-cols-2 lg:grid-cols-3">
              <div
                className={`flex flex-col p-4 space-y-2 gap-4 rounded-sm bg-dbGrey dark:bg-dbNavBackgroundColorDark`}
              >
                <h3 className="font-semibold text-h3 text-dbBlue dark:text-dbWhiteDark">
                  Login to explore all Tribes: Discover the Heart of Our
                  Community
                </h3>
                <p className="text-base text-dbBlue dark:text-dbWhiteDark">
                  Dive into the diverse and vibrant tapestry of tribes that form
                  the backbone of our organization. Explore their unique
                  cultures, values, and missions, and learn how they contribute
                  to our shared vision.
                </p>
              </div>
              <div
                className={`flex flex-col p-4 space-y-2 gap-4 rounded-sm bg-dbSand dark:bg-dbNavBackgroundColorDark`}
              >
                <h3 className="font-semibold text-h3 text-dbBlue dark:text-dbWhiteDark">
                  Login to browse all Squads: Unveil the Teams Shaping Our
                  Future
                </h3>
                <p className="text-base text-dbBlue dark:text-dbWhiteDark">
                  Get acquainted with the dynamic squads that are driving
                  innovation and shaping our future. From cross-functional teams
                  to specialized units, discover how these squads collaborate
                  and deliver impactful outcomes.
                </p>
              </div>
              <div
                className={`flex flex-col p-4 space-y-2 gap-4 rounded-sm bg-dbSand dark:bg-dbNavBackgroundColorDark`}
              >
                <h3 className="font-semibold text-h3 text-dbBlue dark:text-dbWhiteDark">
                  Login to discover all Products: Dive into Our Innovation
                  Portfolio
                </h3>
                <p className="text-base text-dbBlue dark:text-dbWhiteDark">
                  Explore our extensive innovation portfolio and delve into the
                  exciting projects that are pushing boundaries and driving
                  progress across the organization. From initiatives to
                  improvements, see how we're making an impact.
                </p>
              </div>
            </div>
          </section>
        </div>
      </div>
    );
  }

  return (
    <div className="bg-dbBackgroundColor dark:bg-dbBackgroundColorDark">
      {!loggedIn && (
        <PageInfo
          title="Welcome to"
          secondTitle="BOREDK Spotlight Platform"
          content="BOREDK Spotlight Platform is Danske Bank's new internal release hub. The platform is designed to be your central source for internal knowledge sharing. Here you can follow what is happening throughout Danske Bank, from product development to business analyses. Our goal is to create an open and informative platform that makes it easy for all employees to stay updated and collaborate across departments. Login to access the platform data."
        />
      )}
      {loggedIn && (
        <section className="mb-16">
          {" "}
          <p className="pt-8 pl-8 text-5xl font-medium text-dbBlue dark:text-dbWhite">
            Hello {user.user_first_name} {user.user_last_name}!
          </p>
          <h1 className="pt-10 pl-8 font-medium text-h1 text-dbBlue dark:text-dbWhite">
            Welcome to BOREDK Spotlight Platform
          </h1>
          <p></p>
        </section>
      )}
      <Section
        bgColorLightMode="bg-dbBlue"
        bgColorDarkMode="bg-dbCardBackgroundColorDark"
      >
        <StatisticsOverview />
      </Section>
      <Section
        bgColorLightMode="bg-dbWhite"
        bgColorDarkMode="bg-dbBackgroundColorDark"
      >
        <div className="px-4">
          <h2 className="mb-8 font-medium text-center text-h2 text-dbBlue dark:text-dbWhiteDark">
            Newly released products
          </h2>
          <div className="grid flex-wrap justify-center grid-cols-1 gap-4 md:grid-cols-2 lg:grid-cols-4">
            {productsLoading ? (
              <p>Loading...</p>
            ) : productsError ? (
              <p className="text-center text-red-500">{productsError}</p>
            ) : (
              products.map((product) => (
                <ProductCard key={product.product_title} product={product} />
              ))
            )}
          </div>
          <div className="flex justify-center my-8">
            <PrimaryButton content="See all products" linkTo="/products" />
          </div>
        </div>
      </Section>
      <section>
        <div className="px-4 py-12 bg-dbLightBlue dark:bg-dbCardBackgroundColorDark">
          <h2 className="mb-8 font-medium text-center text-h2 text-dbBlue dark:text-dbWhiteDark">
            All Squads in Danske Bank
          </h2>
          <div className="grid gap-5 sm:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4">
            {squadsLoading ? (
              <p>Loading...</p>
            ) : squadsError ? (
              <p className="text-center text-red-500">{squadsError}</p>
            ) : (
              squads.map((squad) => (
                <SquadCard key={squad.squad_name} squad={squad} />
              ))
            )}
          </div>
          <div className="flex justify-center my-8">
            <PrimaryButton content="See all Squads" linkTo="/squads" />
          </div>
        </div>
      </section>
      <InfoBoxSection />
      <section className="mt-4 bg-dbBlue dark:bg-dbBackgroundColorDark">
        <div className="px-4 py-12">
          <h2 className="mb-8 font-medium text-center text-h2 text-dbSand ">
            Latest updates
          </h2>
          <div className="grid flex-wrap justify-center grid-cols-1 gap-4 md:grid-cols-2 lg:grid-cols-4">
            {productsLoading ? (
              <p>Loading...</p>
            ) : productsError ? (
              <p className="text-center text-red-500">{productsError}</p>
            ) : (
              products.map((product) => (
                <ProductCard key={product.product_title} product={product} />
              ))
            )}
          </div>
        </div>
      </section>
    </div>
  );
};

export default Home;
