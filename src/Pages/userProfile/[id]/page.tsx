import React from "react";
import Section from "../../../Layouts/section/page";
import PrimaryButton from "../../../Components/Buttons/primaryBtn/page";
import InfoBoxSection from "../../../Layouts/infoBox/page";
import useFetchProducts from "../../../Hooks/useFetchProducts";
import useFetchCookieUser from "../../../Hooks/useFetchCookieUser";
import useFetchRoleName from "../../../Hooks/useFetchRoleName";
import ProductCard from "../../../Components/Cards/productCard/page";
import { FaRegEdit } from "react-icons/fa";
import useFetchSquadName from "../../../Hooks/useFetchSquadName";
import person_placeholder from "../../../Assets/images/person_placeholder.svg";

const UserProfile: React.FC = () => {
  const { user } = useFetchCookieUser();
  const {
    products,
    loading: productsLoading,
    error: productsError,
  } = useFetchProducts(4);

  const squadId = user?.user_squad_fk ? Number(user.user_squad_fk) : undefined;

  const { roleName } = useFetchRoleName();
  const { squadName } = useFetchSquadName();

  if (!user) {
    return <p>Loading...</p>;
  }

  return (
    <Section
      bgColorLightMode="bg-dbBackgroundColor"
      bgColorDarkMode="bg-dbBackgroundColorDark"
    >
      <div className="p-8">
        <h1 className="mb-8 font-medium text-h1 dark:text-dbWhiteDark ">
          {user.user_first_name} {user.user_last_name}
        </h1>
        <section className="flex items-center">
          <div className="mr-8">
            <img
              src={person_placeholder}
              alt="Person Placeholder"
              className="h-56 mr-8"
            />
          </div>
          <table className="w-full table-auto text-dbBlue dark:text-dbWhite">
            <tbody>
              <TableRow title="Username" value={user.user_username} />
              <TableRow title="Password" value="******" />
              <TableRow title="Email" value={user.user_email} />
              <TableRow title="Role" value={roleName} />
              <TableRow title="Part of Squad" value={squadName} />
            </tbody>
          </table>
        </section>
      </div>
      <Section
        bgColorLightMode="bg-dbLightBlue"
        bgColorDarkMode="bg-dbCardBackgroundColorDark"
      >
        <div className="px-4">
          <h2 className="mb-8 font-medium text-center text-h2 text-dbBlue dark:text-dbWhiteDark">
            Part of projects
          </h2>
          <div className="grid grid-cols-1 gap-4 md:grid-cols-2 lg:grid-cols-4">
            {productsLoading ? (
              <p>Loading...</p>
            ) : productsError ? (
              <p className="text-center text-red-500">{productsError}</p>
            ) : (
              products.map((product) => (
                <ProductCard key={product.product_title} product={product} />
              ))
            )}
          </div>
          <div className="flex justify-center my-8">
            <PrimaryButton content="See all products" linkTo="/products" />
          </div>
        </div>
      </Section>
      <InfoBoxSection />
    </Section>
  );
};

interface TableRowProps {
  title: string;
  value: string | number;
}

const TableRow: React.FC<TableRowProps> = ({ title, value }) => (
  <tr className="border-b border-dbGrey dark:border-dbGrey">
    <td className="px-4 py-2 font-semibold">{title}</td>
    <td className="px-4 py-2">{value}</td>
    <td className="px-4 py-2">
      <button className="text-blue-500 hover:text-blue-700">
        <FaRegEdit />
      </button>
    </td>
  </tr>
);

export default UserProfile;
