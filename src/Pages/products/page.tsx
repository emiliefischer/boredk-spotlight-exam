import React, { useEffect, useState } from "react";
import Section from "../../Layouts/section/page";
import PageInfo from "../../Components/pageInfo/page";
import SearchFieldOverviews from "../../Components/searchOverviews/page";
import PrimaryButton from "../../Components/Buttons/primaryBtn/page";
import ProductCard from "../../Components/Cards/productCard/page";
import useFetchProducts from "../../Hooks/useFetchProducts";
import { getStatusColorClass } from "../../Utils/statusUtils";
import FilterField from "../../Components/filterField/page";

const AllProducts: React.FC = () => {
  const {
    products,
    loading: productsLoading,
    error: productsError,
  } = useFetchProducts();

  const [filteredProducts, setFilteredProducts] = useState(products);
  const [searchTerm, setSearchTerm] = useState("");
  const [statusFilter, setStatusFilter] = useState(""); // State to hold status filter

  useEffect(() => {
    setFilteredProducts(products);
  }, [products]);

  const handleSearch = (searchTerm: string) => {
    setSearchTerm(searchTerm);
    applyFilters(searchTerm, statusFilter);
  };

  const handleStatusFilter = (selectedStatus: string) => {
    setStatusFilter(selectedStatus);
    applyFilters(searchTerm, selectedStatus);
  };

  const applyFilters = (searchTerm: string, status: string) => {
    let filtered = products;

    // Tilføj search filter
    if (searchTerm.trim() !== "") {
      const lowerCaseTerm = searchTerm.toLowerCase();
      filtered = filtered.filter((product) =>
        product.product_title.toLowerCase().includes(lowerCaseTerm)
      );
    }

    // Tilføj status filter
    if (status !== "") {
      filtered = filtered.filter(
        (product) => product.status_name.toLowerCase() === status.toLowerCase()
      );
    }

    setFilteredProducts(filtered);
  };

  return (
    <div className=" bg-dbBackgroundColor dark:bg-dbBackgroundColorDark">
      <PageInfo
        title="About"
        secondTitle="Our Products in Danske Bank"
        content="Embark on a journey through the diverse landscape of Products within Danske Bank! Here, innovation thrives, and initiatives take shape to transform ideas into reality. Explore a plethora of Products, each a testament to creativity, collaboration, and ingenuity. From cutting-edge technology ventures to transformative business endeavors, this is your gateway to the dynamic world of Products within Danske Bank."
      />

      <Section
        bgColorLightMode="bg-dbBackgroundColor"
        bgColorDarkMode="bg-dbCardBackgroundColorDark"
      >
        <div className="px-4">
          <div className="flex justify-between pb-4">
            <SearchFieldOverviews onSearch={handleSearch} />
            <FilterField
              options={["New", "In Progress", "Closed"]}
              onSelect={handleStatusFilter}
            />
          </div>
          <div className="grid gap-5 sm:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4">
            {productsLoading ? (
              <p>Loading...</p>
            ) : productsError ? (
              <p className="text-center text-red-500">{productsError}</p>
            ) : (
              filteredProducts.map((product) => (
                <ProductCard key={product.product_title} product={product} />
              ))
            )}
          </div>
          <div className="flex justify-center my-8">
            <PrimaryButton content="Show all products" linkTo="/products" />
          </div>
        </div>
      </Section>
      <Section
        bgColorLightMode="bg-dbBlue"
        bgColorDarkMode="bg-dbBackgroundColorDark"
      >
        <div className="px-4">
          <h2 className="mb-8 font-medium text-center text-h2 text-dbSand ">
            Newly released products
          </h2>
          <div className="grid flex-wrap justify-center grid-cols-1 gap-4 md:grid-cols-2 lg:grid-cols-4">
            {productsLoading ? (
              <p>Loading...</p>
            ) : productsError ? (
              <p className="text-center text-red-500">{productsError}</p>
            ) : (
              products.map((product) => (
                <ProductCard key={product.product_title} product={product} />
              ))
            )}
          </div>
        </div>
      </Section>
    </div>
  );
};

export default AllProducts;
