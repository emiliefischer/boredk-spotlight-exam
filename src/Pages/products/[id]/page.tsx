import React, { useEffect, useState } from "react";
import PageInfo from "../../../Components/pageInfo/page";
import Section from "../../../Layouts/section/page";
import SearchField from "../../../Components/searchField/page";
import PrimaryButton from "../../../Components/Buttons/primaryBtn/page";
import FilterField from "../../../Components/filterField/page";
import projectImg from "../../../Assets/images/projectImg.png";
import github from "../../../Assets/images/github.svg";
import figma from "../../../Assets/images/figma.svg";
import hosting from "../../../Assets/images/hosting.svg";
import confluence from "../../../Assets/images/confluence.svg";
import ProductLinkCard from "../../../Components/productLinkCard/page";
import ProductCard from "../../../Components/Cards/productCard/page";
import useFetchProducts from "../../../Hooks/useFetchProducts";
import { useParams } from "react-router-dom";
import { Product } from "../../../Types/productInterface";
import axios from "axios";

const SingleProject = () => {
  const {
    products,
    loading: productsLoading,
    error: productsError,
  } = useFetchProducts(4);

  const { productTitle } = useParams<{ productTitle: string }>();
  const [product, setProduct] = useState<Product | null>(null);

  useEffect(() => {
    const fetchProduct = async () => {
      try {
        const baseUrl =
          process.env.NODE_ENV === "development"
            ? process.env.REACT_APP_API_BASE_URL_LOCAL
            : process.env.REACT_APP_API_BASE_URL_PROD;

        let apiUrl;
        if (process.env.NODE_ENV === "development") {
          apiUrl = `${baseUrl}/products/${productTitle}`;
        } else {
          apiUrl = `${baseUrl}/productsDataRoute/${productTitle}`;
        }

        const response = await axios.get<Product>(apiUrl);
        setProduct(response.data);
      } catch (error) {
        console.error("Error fetching tribe:", error);
      }
    };
    fetchProduct();
  }, [productTitle]);

  if (!product) {
    return <div>Loading...</div>;
  }

  return (
    <div className=" bg-dbBackgroundColor dark:bg-dbBackgroundColorDark">
      <div className="grid lg:grid-cols-2">
        <PageInfo
          title="About"
          secondTitle={product.product_title}
          content={product.product_description}
        />
        <div className="flex justify-center p-12">
          {product.product_image ? (
            <img
              className="max-h-[400px] object-cover"
              src={product.product_image}
              alt={product.product_title}
            />
          ) : (
            <img src={projectImg} alt="Default Image" />
          )}
        </div>
      </div>
      {/* <Section
        bgColorLightMode="bg-dbGrey"
        bgColorDarkMode="bg-dbCardBackgroundColorDark"
      >
        <h2 className="mb-8 font-medium text-center text-h2 text-dbBlue dark:text-dbWhiteDark">
          Product members
        </h2>
      </Section> */}
      <Section
        bgColorLightMode="bg-dbBackgroundColor"
        bgColorDarkMode="bg-dbBackgroundColorDark"
      >
        <h2 className="mb-8 font-medium text-center text-h2 text-dbBlue dark:text-dbSand">
          Link to project
        </h2>
        <div className="justify-between p-12 lg:flex">
          <div className="flex items-center justify-center flex-1 ">
            <ProductLinkCard image={github} link="Link to Github" />
          </div>
          <div className="flex items-center justify-center flex-1 ">
            <ProductLinkCard image={figma} link="Link to Figma" />
          </div>
          <div className="flex items-center justify-center flex-1 ">
            <ProductLinkCard image={hosting} link="Link to Hosting" />
          </div>
          <div className="flex items-center justify-center flex-1 ">
            <ProductLinkCard image={confluence} link="Link to Confluence" />
          </div>
        </div>
      </Section>
      <Section
        bgColorLightMode="bg-dbLightBlue"
        bgColorDarkMode="bg-dbCardBackgroundColorDark "
      >
        <div className="px-4">
          <h2 className="mb-8 font-medium text-center text-h2 text-dbBlue dark:text-dbWhiteDark">
            Other products made by same Squad
          </h2>
          <div className="grid gap-5 pb-8 sm:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4">
            <SearchField />
            {/* <FilterField /> */}
          </div>
          <div className="grid gap-5 sm:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4">
            {productsLoading ? (
              <p>Loading...</p>
            ) : productsError ? (
              <p className="text-center text-red-500">{productsError}</p>
            ) : (
              products.map((product) => (
                <ProductCard key={product.product_title} product={product} />
              ))
            )}
          </div>
          <div className="flex justify-center my-8">
            <PrimaryButton content="Show all projects" linkTo="#" />
          </div>
        </div>
      </Section>
      <section className="mt-4 bg-dbBlue dark:bg-dbBackgroundColorDark">
        <div className="px-4 py-12">
          <h2 className="mb-8 font-medium text-center text-h2 text-dbSand ">
            Latest updates
          </h2>
          <div className="grid flex-wrap justify-center grid-cols-1 gap-4 md:grid-cols-2 lg:grid-cols-4">
            {productsLoading ? (
              <p>Loading...</p>
            ) : productsError ? (
              <p className="text-center text-red-500">{productsError}</p>
            ) : (
              products.map((product) => (
                <ProductCard key={product.product_title} product={product} />
              ))
            )}
          </div>
        </div>
      </section>
    </div>
  );
};

export default SingleProject;
