import React, { useState, useEffect } from "react";
import TribeCard from "../../Components/Cards/tribeCard/page";
import Section from "../../Layouts/section/page";
import PageInfo from "../../Components/pageInfo/page";
import PrimaryButton from "../../Components/Buttons/primaryBtn/page";
import InfoBoxSection from "../../Layouts/infoBox/page";
import FilterField from "../../Components/filterField/page";
import useFetchTribes from "../../Hooks/useFetchTribes";
import SearchFieldOverviews from "../../Components/searchOverviews/page";

const AllTribes: React.FC = () => {
  const {
    tribes,
    loading: tribesLoading,
    error: tribesError,
  } = useFetchTribes(16);
  const [filteredTribes, setFilteredTribes] = useState(tribes);

  useEffect(() => {
    setFilteredTribes(tribes);
  }, [tribes]);

  const handleSearch = (searchTerm: string) => {
    if (searchTerm.trim() === "") {
      setFilteredTribes(tribes);
    } else {
      const lowerCaseTerm = searchTerm.toLowerCase();
      setFilteredTribes(
        tribes.filter((tribe) =>
          tribe.tribe_name.toLowerCase().includes(lowerCaseTerm)
        )
      );
    }
  };

  return (
    <div className="bg-dbBackgroundColor dark:bg-dbBackgroundColorDark">
      <PageInfo
        title="About"
        secondTitle="Our Tribes in Danske Bank"
        content="Welcome to the vibrant world of Tribes within Danske Bank! This is your gateway to exploring the diverse communities that form the backbone of our organization. Here, you can discover a rich tapestry of Tribes, each with its own unique culture, values, and purpose. From technology enthusiasts to sustainability advocates, this is where you can see an overview of all Tribes and learn how they collaborate, innovate, and drive positive change within and beyond Danske Bank."
      />
      <Section
        bgColorLightMode="bg-dbLightBlue"
        bgColorDarkMode="bg-dbCardBackgroundColorDark"
      >
        <div className="px-4">
          <h2 className="mb-8 font-medium text-center text-h2 text-dbBlue dark:text-dbWhiteDark">
            Overview of all Tribes
          </h2>
          <div className="grid gap-5 pb-8 sm:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4">
            <SearchFieldOverviews onSearch={handleSearch} />
            {/* <FilterField /> */}
          </div>
          {tribesError ? (
            <p className="text-center text-red-500">{tribesError}</p>
          ) : (
            <div className="grid gap-5 sm:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4">
              {tribesLoading ? (
                <p>Loading...</p>
              ) : (
                filteredTribes.map((tribe) => (
                  <TribeCard key={tribe.tribe_name} tribe={tribe} />
                ))
              )}
            </div>
          )}
          <div className="flex justify-center my-8">
            <PrimaryButton content="Load all Tribes" linkTo="/tribes" />
          </div>
        </div>
      </Section>
      <InfoBoxSection />
    </div>
  );
};

export default AllTribes;
