import React, { useEffect, useState } from "react";
import PageInfo from "../../Components/pageInfo/page";
import Section from "../../Layouts/section/page";
import AdminCard from "../../Components/Cards/adminCard/page";
import List from "../../Components/Cards/listCard/page";
import useCountTribes from "../../Hooks/useCountTribes";
import useCountProducts from "../../Hooks/useCountProduct";
import useCountUsers from "../../Hooks/useCountUsers";
import useCountSquads from "../../Hooks/useCountSquads";
import useFetchTribes from "../../Hooks/useFetchTribes";
import useFetchSquads from "../../Hooks/useFetchSquads";
import useFetchProducts from "../../Hooks/useFetchProducts";
import useFetchUsers from "../../Hooks/useFetchUsers";
import useCountSquadsByTribe from "../../Hooks/useCountSquadByTribe";

const AdminDashboard: React.FC = () => {
  const [selectedCard, setSelectedCard] = useState<string | null>(null);
  const [selectedSection, setSelectedSection] = useState<string | null>(null);
  const [userRoles, setUserRoles] = useState({
    isSuperiorAdmin: false,
    isSquadAdmin: false,
    isTribeAdmin: false,
  });
  const [tribeId] = useState<string | null>(null);

  useEffect(() => {
    const roles = JSON.parse(localStorage.getItem("userRoles") || "{}");
    setUserRoles(roles);
  }, []);

  const { tribeCount, loading: tribeLoading } = useCountTribes();
  const { productCount, loading: productLoading } = useCountProducts();
  const { userCount, loading: userLoading } = useCountUsers();
  const { squadCount, loading: squadLoading } = useCountSquads();
  const { squadCount: tribeSquadCount, loading: tribeSquadLoading } =
    useCountSquadsByTribe(tribeId!);

  const {
    tribes,
    loading: tribesLoading,
    error: tribesError,
  } = useFetchTribes();
  const {
    squads,
    loading: squadsLoading,
    error: squadsError,
  } = useFetchSquads();
  const {
    products,
    loading: productsLoading,
    error: productsError,
  } = useFetchProducts();
  const { users, loading: usersLoading, error: usersError } = useFetchUsers();

  // Map data til List component
  const mappedTribesData = tribes.map((tribe) => ({
    id: tribe.tribe_id,
    name: tribe.tribe_name,
    description: tribe.tribe_description,
    total: tribe.total_squads,
  }));
  const mappedSquadData = squads.map((squad) => ({
    id: squad.squad_id,
    name: squad.squad_name,
    description: squad.squad_description,
    total: squad.total_products,
  }));
  const mappedProductData = products.map((product) => ({
    id: product.product_id,
    name: product.product_title,
    description: product.product_description,
  }));
  const mappedUserData = users.map((user) => ({
    id: user.user_id,
    name: user.user_username,
    description: user.user_email,
  }));

  const handleCardSelect = (cardTitle: string) => {
    setSelectedCard(cardTitle);
    switch (cardTitle) {
      case "Total Tribes":
        setSelectedSection("Tribes");
        break;
      case "Total Squads":
        setSelectedSection("Squads");
        break;
      case "Total Products":
        setSelectedSection("Products");
        break;
      case "Total Users":
        setSelectedSection("Users");
        break;
      default:
        setSelectedSection(null);
    }
  };

  const renderList = () => {
    switch (selectedSection) {
      case "Tribes":
        if (tribesLoading) {
          return <p>Loading tribes...</p>;
        } else if (tribesError) {
          return <p>Error fetching tribes: {tribesError}</p>;
        } else {
          return (
            <List
              title="Tribes"
              th1="ID"
              th2="Name"
              th3="Description"
              th4="Total Squads"
              adminCard="Tribes"
              data={mappedTribesData}
              secondaryBtnContent="Add Tribe"
              secondaryBtnLink="/create-tribe"
              primaryBtnContent="Save Tribes"
              primaryBtnLink="/save-tribes"
            />
          );
        }
      case "Squads":
        if (squadsLoading) {
          return <p>Loading squads...</p>;
        } else if (squadsError) {
          return <p>Error fetching squads: {squadsError}</p>;
        } else {
          return (
            <List
              title="Squads"
              th1="ID"
              th2="Name"
              th3="Description"
              th4="Total Products"
              adminCard="Squads"
              data={mappedSquadData}
              secondaryBtnContent="Add Squad"
              secondaryBtnLink="/create-squad"
              primaryBtnContent="Save Squad"
              primaryBtnLink="/save-squad"
            />
          );
        }
      case "Products":
        if (productsLoading) {
          return <p>Loading products...</p>;
        } else if (productsError) {
          return <p>Error fetching products: {productsError}</p>;
        } else {
          return (
            <List
              title="Products"
              th1="ID"
              th2="Name"
              th3="Description"
              adminCard="Products"
              data={mappedProductData}
              secondaryBtnContent="Add Product"
              secondaryBtnLink="/create-product"
              primaryBtnContent="Save Product"
              primaryBtnLink="/save-product"
            />
          );
        }
      case "Users":
        if (usersLoading) {
          return <p>Loading users...</p>;
        } else if (usersError) {
          return <p>Error fetching users: {usersError}</p>;
        } else {
          return (
            <List
              title="Users"
              th1="ID"
              th2="Username"
              th3="Email"
              adminCard="Users"
              data={mappedUserData}
              secondaryBtnContent="Add User"
              secondaryBtnLink="/create-user"
              primaryBtnContent="Save User"
              primaryBtnLink="/save-user"
            />
          );
        }
      default:
        return null;
    }
  };

  return (
    <div className="bg-dbBackgroundColor dark:bg-dbBackgroundColorDark">
      {userRoles.isSuperiorAdmin ? (
        <div>
          <PageInfo
            title=""
            secondTitle="Superior Admin Dashboard"
            content="Explore and manage all aspects of our organization from one centralized dashboard. Edit Tribes, Squads, Products, and user profiles effortlessly to drive efficiency and collaboration across teams."
          />
          <div className="px-4">
            <Section
              bgColorLightMode="bg-dbBackgroundColor"
              bgColorDarkMode="bg-dbCardBackgroundColorDark"
            >
              <div className="grid gap-5 md:grid-cols-2 xl:grid-cols-4 max-w-[1200px]">
                <AdminCard
                  title="Total Tribes"
                  number={
                    tribeLoading
                      ? "Loading..."
                      : tribeCount !== null
                      ? tribeCount.toString()
                      : "N/A"
                  }
                  isSelected={selectedCard === "Total Tribes"}
                  onSelect={() => handleCardSelect("Total Tribes")}
                />
                <AdminCard
                  title="Total Squads"
                  number={
                    squadLoading
                      ? "Loading..."
                      : squadCount !== null
                      ? squadCount.toString()
                      : "N/A"
                  }
                  isSelected={selectedCard === "Total Squads"}
                  onSelect={() => handleCardSelect("Total Squads")}
                />
                <AdminCard
                  title="Total Products"
                  number={
                    productLoading
                      ? "Loading..."
                      : productCount !== null
                      ? productCount.toString()
                      : "N/A"
                  }
                  isSelected={selectedCard === "Total Products"}
                  onSelect={() => handleCardSelect("Total Products")}
                />
                <AdminCard
                  title="Total Users"
                  number={
                    userLoading
                      ? "Loading..."
                      : userCount !== null
                      ? userCount.toString()
                      : "N/A"
                  }
                  isSelected={selectedCard === "Total Users"}
                  onSelect={() => handleCardSelect("Total Users")}
                />
              </div>
            </Section>
            {selectedSection && (
              <Section
                bgColorLightMode="bg-dbBackgroundColor"
                bgColorDarkMode="bg-dbCardBackgroundColorDark"
              >
                {renderList()}
              </Section>
            )}
          </div>
        </div>
      ) : userRoles.isTribeAdmin ? (
        <div>
          <PageInfo
            title=""
            secondTitle="Tribe Admin Dashboard"
            content="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed semper
          tempor lacus, a maximus metus pretium in. Duis mi mauris, egestas et
          arcu sit amet, tincidunt efficitur augue. Integer varius scelerisque
          justo ac semper."
          />{" "}
          <div className="px-4">
            <Section
              bgColorLightMode="bg-dbBackgroundColor"
              bgColorDarkMode="bg-dbCardBackgroundColorDark"
            >
              <div className="grid gap-5 md:grid-cols-2 xl:grid-cols-4 max-w-[1200px]">
                <AdminCard
                  title="Total Squads"
                  number={
                    tribeSquadLoading
                      ? "Loading..."
                      : tribeSquadCount !== null
                      ? tribeSquadCount.toString()
                      : "N/A"
                  }
                  isSelected={selectedCard === "Total Squads"}
                  onSelect={() => handleCardSelect("Total Squads")}
                />
                <AdminCard
                  title="Total Products"
                  number={
                    productLoading
                      ? "Loading..."
                      : productCount !== null
                      ? productCount.toString()
                      : "N/A"
                  }
                  isSelected={selectedCard === "Total Products"}
                  onSelect={() => handleCardSelect("Total Products")}
                />
                <AdminCard
                  title="Total Employees"
                  number={
                    userLoading
                      ? "Loading..."
                      : userCount !== null
                      ? userCount.toString()
                      : "N/A"
                  }
                  isSelected={selectedCard === "Total Users"}
                  onSelect={() => handleCardSelect("Total Users")}
                />
              </div>
            </Section>
            {selectedSection && (
              <Section
                bgColorLightMode="bg-dbBackgroundColor"
                bgColorDarkMode="bg-dbCardBackgroundColorDark"
              >
                {renderList()}
              </Section>
            )}
          </div>
        </div>
      ) : userRoles.isSquadAdmin ? (
        <div>
          <PageInfo
            title=""
            secondTitle="Squad Admin Dashboard"
            content="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed semper
          tempor lacus, a maximus metus pretium in. Duis mi mauris, egestas et
          arcu sit amet, tincidunt efficitur augue. Integer varius scelerisque
          justo ac semper."
          />{" "}
          <div className="px-4">
            <Section
              bgColorLightMode="bg-dbBackgroundColor"
              bgColorDarkMode="bg-dbCardBackgroundColorDark"
            >
              <div className="grid gap-5 md:grid-cols-2 xl:grid-cols-4 max-w-[1200px]">
                <AdminCard
                  title="Total Tribes"
                  number={
                    tribeLoading
                      ? "Loading..."
                      : tribeCount !== null
                      ? tribeCount.toString()
                      : "N/A"
                  }
                  isSelected={selectedCard === "Total Tribes"}
                  onSelect={() => handleCardSelect("Total Tribes")}
                />
                <AdminCard
                  title="Total Squads"
                  number={
                    squadLoading
                      ? "Loading..."
                      : squadCount !== null
                      ? squadCount.toString()
                      : "N/A"
                  }
                  isSelected={selectedCard === "Total Squads"}
                  onSelect={() => handleCardSelect("Total Squads")}
                />
                <AdminCard
                  title="Total Products"
                  number={
                    productLoading
                      ? "Loading..."
                      : productCount !== null
                      ? productCount.toString()
                      : "N/A"
                  }
                  isSelected={selectedCard === "Total Products"}
                  onSelect={() => handleCardSelect("Total Products")}
                />
                <AdminCard
                  title="Total Users"
                  number={
                    userLoading
                      ? "Loading..."
                      : userCount !== null
                      ? userCount.toString()
                      : "N/A"
                  }
                  isSelected={selectedCard === "Total Users"}
                  onSelect={() => handleCardSelect("Total Users")}
                />
              </div>
            </Section>
            {selectedSection && (
              <Section
                bgColorLightMode="bg-dbBackgroundColor"
                bgColorDarkMode="bg-dbCardBackgroundColorDark"
              >
                {renderList()}
              </Section>
            )}
          </div>
        </div>
      ) : null}
    </div>
  );
};

export default AdminDashboard;
