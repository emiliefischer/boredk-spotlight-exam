import React from "react";
import "flowbite";
import { Routes, Route } from "react-router-dom";
import AllTribes from "./Pages/tribes/page";
import AllSquads from "./Pages/squads/page";
import AllProducts from "./Pages/products/page";
import Home from "./Pages/landingPage/page";
import RootLayout from "./Layouts/rootLayout/page";
import LoginModalPopup from "./Components/loginModal/page";
import SingleProject from "./Pages/products/[id]/page";
import SingleSquad from "./Pages/squads/[id]/page";
import SingleTribe from "./Pages/tribes/[id]/page";
import AdminDashboard from "./Pages/adminDashboard/page";
import CreateProduct from "./Pages/createProduct/page";
import CreateSquad from "./Pages/createSquad/page";
import CreateTribe from "./Pages/createTribe/page";
import CreateUserForm from "./Pages/createUser/page";
import UpdateTribe from "./Pages/updateTribe/page";
import ProtectedRoute from "./Components/protectedRoute/page";
import UserProtectedRoute from "./Components/protectedUserRoute/page";
import UserProfile from "./Pages/userProfile/[id]/page";
import UserFavorites from "./Pages/userFavorites/page";

function App() {
  function useParams<T>() {
    throw new Error("Function not implemented.");
  }

  return (
    <RootLayout>
      <LoginModalPopup />
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/products" element={<AllProducts />} />
        <Route path="/tribes" element={<AllTribes />} />
        <Route path="/squads" element={<AllSquads />} />
        <Route path="/favorites" element={<UserFavorites />} />
        <Route path="/tribes/:tribeName" element={<SingleTribe />} />
        <Route path="/squads/:squadName" element={<SingleSquad />} />
        <Route path="/products/:productTitle" element={<SingleProject />} />
        <Route path="/admindashboard" element={<AdminDashboard />} />
        <Route path="/create-user" element={<CreateUserForm />} />
        <Route path="/userprofile" element={<UserProfile />} />
        <Route path="/create-tribe" element={<CreateTribe />} />
        <Route path="/create-squad" element={<CreateSquad />} />
        <Route path="/create-product" element={<CreateProduct />} />
        {/* <Route path="/update-tribe/:tribeId" element={<UpdateTribe />} /> */}
        <Route
          path="/admindashboard"
          element={<ProtectedRoute component={AdminDashboard} />}
        />
        <Route
          path="/create-user"
          element={<ProtectedRoute component={CreateUserForm} />}
        />
        <Route
          path="/create-tribe"
          element={<ProtectedRoute component={CreateTribe} />}
        />
        <Route
          path="/create-squad"
          element={<ProtectedRoute component={CreateSquad} />}
        />
        <Route
          path="/create-product"
          element={<ProtectedRoute component={CreateProduct} />}
        />
        <Route
          path="/update-tribe/:tribeId"
          element={<ProtectedRoute component={UpdateTribe} />}
        />
        <Route
          path="/products"
          element={<UserProtectedRoute component={AllProducts} />}
        />
      </Routes>
    </RootLayout>
  );
}
export default App;
