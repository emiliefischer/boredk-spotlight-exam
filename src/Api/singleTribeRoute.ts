const express = require("express");
const router = express.Router();
const { executeQuery } = require("../../mySQLConnect");
import { Response, Request } from "express";

router.get("/:tribeName", async (req: Request, res: Response) => {
  const { tribeName } = req.params;
  try {
    console.log(`Fetching tribe with name: ${tribeName}`);
    const query = `
      SELECT * FROM tribes WHERE LOWER(REPLACE(tribe_name, ' ', '-')) = ?
    `;
    const tribes = await executeQuery(query, [tribeName]);
    if (tribes.length > 0) {
      console.log("Tribe fetched:", tribes[0]);
      res.json(tribes[0]);
    } else {
      res.status(404).json({ error: "Tribe not found" });
    }
  } catch (error) {
    console.error("Error retrieving tribe:", error);
    res.status(500).json({ error: "Internal server error" });
  }
});

module.exports = router;
