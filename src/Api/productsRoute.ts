import { builder, Handler } from "@netlify/functions";
const { executeQuery } = require("../../mySQLConnect");

const myHandler: Handler = async (event, context) => {
  try {
    console.log("Fetching products from the database...");
    const query = `
      SELECT * FROM products
    `;
    const products = await executeQuery(query);
    console.log("Products fetched:", products);

    return {
      statusCode: 200,
      body: JSON.stringify(products),
    };
  } catch (error) {
    console.error("Error retrieving products:", error);
    return {
      statusCode: 500,
      body: JSON.stringify({ error: "Internal server error" }),
    };
  }
};

const handler = builder(myHandler);

export { handler };
