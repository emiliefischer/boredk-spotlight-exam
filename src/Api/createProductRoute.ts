import express, { Request, Response } from "express";
import { v4 as uuidv4 } from "uuid";
import { executeQuery } from "../../mySQLConnect";
import multer from "multer";
import AWS from "aws-sdk";
import path from "path";

const router = express.Router();

// multer til fil-uploads
const storage = multer.memoryStorage();
const upload = multer({
  storage: storage,
  fileFilter: (req, file, cb) => {
    const fileTypes = /jpeg|jpg|png/;
    const extname = fileTypes.test(
      path.extname(file.originalname).toLowerCase()
    );
    const mimeType = fileTypes.test(file.mimetype);

    if (extname && mimeType) {
      return cb(null, true);
    } else {
      cb(new Error("Only .jpeg, .jpg, and .png files are allowed!"));
    }
  },
});

// Konfiguration AWS SDK
AWS.config.update({ region: "eu-north-1" });
const s3 = new AWS.S3();

// Opret nyt product
router.post(
  "/",
  upload.single("image"),
  async (req: Request, res: Response) => {
    try {
      const {
        productTitle,
        productDescription,
        productGithubLink,
        productFigmaLink,
        productHostLink,
        productConfluenceLink,
        productSquadFk,
        productProductOwnerFk,
        productStatusFk,
      } = req.body;

      const productId = uuidv4().replace(/-/g, "");

      if (
        !productTitle ||
        !productDescription ||
        !productSquadFk ||
        !productProductOwnerFk ||
        !productStatusFk
      ) {
        return res.status(400).json({ error: "Missing required fields" });
      }

      let productImage: string | null = null;

      // upload img til S3
      if (req.file) {
        const fileName = `${uuidv4()}${path
          .extname(req.file.originalname)
          .toLowerCase()}`;
        const params = {
          Bucket: "boredkspotlightbucket",
          Key: fileName,
          Body: req.file.buffer,
          ContentType: req.file.mimetype,
        };

        const uploadResult = await s3.upload(params).promise();
        productImage = uploadResult.Location;
      }

      // Query indsæt product i database
      const query = `
      INSERT INTO products (
        product_id,
        product_created_at,
        product_updated_at,
        product_title,
        product_description,
        product_image,
        product_github_link,
        product_figma_link,
        product_host_link,
        product_confluence_link,
        product_squad_fk,
        product_productowner_fk,
        product_status_fk
      )
      VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
    `;

      await executeQuery(query, [
        productId,
        new Date(),
        new Date(),
        productTitle,
        productDescription,
        productImage,
        productGithubLink,
        productFigmaLink,
        productHostLink,
        productConfluenceLink,
        productSquadFk,
        productProductOwnerFk,
        productStatusFk,
      ]);

      res.status(201).json({ message: "Product created successfully" });
    } catch (error) {
      console.error("Error creating product:", error);
      res.status(500).json({ error: "Internal server error" });
    }
  }
);

module.exports = router;
