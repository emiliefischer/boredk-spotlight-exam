import express, { Request, Response } from "express";
import { executeQuery } from "../../mySQLConnect";

const router = express.Router();

// Search endpoint
router.get("/", async (req: Request, res: Response) => {
  try {
    const searchTerm = req.query.q as string;

    if (!searchTerm) {
      return res.status(400).json({ error: "Search term is required" });
    }

    const query = `
      SELECT * FROM (
        SELECT product_id AS id, product_title AS name, 'product' AS type FROM products WHERE product_title LIKE ?
        UNION
        SELECT tribe_id AS id, tribe_name AS name, 'tribe' AS type FROM tribes WHERE tribe_name LIKE ?
        UNION
        SELECT squad_id AS id, squad_name AS name, 'squad' AS type FROM squads WHERE squad_name LIKE ?
      ) AS search_results;
    `;

    const results = await executeQuery(query, [
      `%${searchTerm}%`,
      `%${searchTerm}%`,
      `%${searchTerm}%`,
    ]);

    res.json(results);
  } catch (error) {
    console.error("Error during search:", error);
    res.status(500).json({ error: "Internal server error" });
  }
});

module.exports = router;
