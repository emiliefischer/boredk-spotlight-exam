const express = require("express");
const router = express.Router();
const { executeQuery } = require("../../mySQLConnect");
import { Response, Request } from "express";

// Get count of all new products
router.get("/", async (req: Request, res: Response) => {
  try {
    console.log("Fetching new products from database...");
    const query = `
      SELECT COUNT(products.product_id) AS productsNewCount
      FROM products
      INNER JOIN product_status ON products.product_status_fk = product_status.status_id
      WHERE product_status.status_name = 'new';
    `;
    const result = await executeQuery(query);
    const productsNewCount = result[0].productsNewCount;
    console.log("Count of new products fetched:", productsNewCount);
    res.json({ productsNewCount });
  } catch (error) {
    console.error("Error retrieving count of new products:", error);
    res.status(500).json({ error: "Internal server error" });
  }
});

module.exports = router;
