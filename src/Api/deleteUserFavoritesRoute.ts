import express from "express";
const router = express.Router();
const { executeQuery } = require("../../mySQLConnect");

// Endpoint for at fjerne et like
router.delete("/", async (req, res) => {
  const { user_id, product_id } = req.body;

  if (!user_id || !product_id) {
    return res.status(400).json({ error: "userId or productId is missing" });
  }

  const query =
    "DELETE FROM user_favorites WHERE user_id = ? AND product_id = ?";

  try {
    await executeQuery(query, [user_id, product_id]);
    console.log(
      `Product ${product_id} removed from favorites for user ${user_id}`
    );
    res
      .status(200)
      .json({ message: "Product removed from favorites successfully" });
  } catch (error) {
    console.error(
      `Error removing product ${product_id} from favorites for user ${user_id}:`,
      error
    );
    res.status(500).json({ error: "Internal server error" });
  }
});

module.exports = router;
