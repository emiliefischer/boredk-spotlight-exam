const express = require("express");
const router = express.Router();
const { executeQuery } = require("../../mySQLConnect");
import { Response, Request } from "express";

// Get tribe by its ID
router.get("/:tribeId", async (req: Request, res: Response) => {
  const { tribeId } = req.params;
  try {
    console.log(`Fetching tribe with ID: ${tribeId}`);
    const query = `
    SELECT * FROM tribes WHERE tribe_id = ?
    `;
    const tribes = await executeQuery(query, [tribeId]);
    if (tribes.length > 0) {
      console.log("Tribe fetched:", tribes[0]);
      res.json(tribes[0]);
    } else {
      res.status(404).json({ error: "Tribe not found" });
    }
  } catch (error) {
    console.error("Error retrieving tribe:", error);
    res.status(500).json({ error: "Internal server error" });
  }
});

module.exports = router;
