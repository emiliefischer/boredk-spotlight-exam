const express = require("express");
const router = express.Router();
const { executeQuery } = require("../../mySQLConnect");
import { Response, Request } from "express";

// Get count of all tribes
router.get("/", async (req: Request, res: Response) => {
  try {
    console.log("Fetching tribe count from database...");
    const query = `SELECT COUNT(*) AS tribeCount FROM tribes`;
    const result = await executeQuery(query);
    const tribeCount = result[0].tribeCount;
    console.log("Tribe count fetched:", tribeCount);
    res.json({ tribeCount });
  } catch (error) {
    console.error("Error retrieving tribe count:", error);
    res.status(500).json({ error: "Internal server error" });
  }
});

module.exports = router;
