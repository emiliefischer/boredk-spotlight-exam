import express from "express";
import { executeQuery } from "../../mySQLConnect";

const router = express.Router();

router.delete("/:productId", async (req, res) => {
  try {
    const { productId } = req.params;

    const query = `
      DELETE FROM products WHERE product_id = ?
    `;

    await executeQuery(query, [productId]);

    res.status(200).json({ message: "Product deleted successfully" });
  } catch (error) {
    console.error("Error deleting product:", error);
    res.status(500).json({ error: "Internal server error" });
  }
});

module.exports = router;
