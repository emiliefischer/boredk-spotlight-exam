import express from "express";
const router = express.Router();
const { executeQuery } = require("../../mySQLConnect");

router.get("/:user_id/favorites", async (req, res) => {
  const { user_id } = req.params;

  if (!user_id) {
    return res.status(400).json({ error: "userId parameter is missing" });
  }

  const query = `
    SELECT 
      p.*, 
      ps.status_name,
      CONCAT(u.user_first_name, ' ', u.user_last_name) AS product_owner_name
    FROM user_favorites uf
    JOIN products p ON uf.product_id = p.product_id
    LEFT JOIN product_status ps ON p.product_status_fk = ps.status_id
    LEFT JOIN users u ON p.product_productowner_fk = u.user_id
    WHERE uf.user_id = ?
  `;

  try {
    const favoriteProducts = await executeQuery(query, [user_id]);
    res.status(200).json({ favoriteProducts });
  } catch (error) {
    console.error(
      `Error fetching favorite products for user ${user_id}:`,
      error
    );
    res.status(500).json({ error: "Internal server error" });
  }
});

module.exports = router;
