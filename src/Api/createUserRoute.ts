import express, { Request, Response } from "express";
import { v4 as uuidv4 } from "uuid";
import bcrypt from "bcrypt";

const router = express.Router();
const { executeQuery } = require("../../mySQLConnect");

const validatePassword = (user_password: string): string | null => {
  const regex = /^(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*[^A-Za-z0-9]).{8,}$/;
  if (!regex.test(user_password)) {
    return "Password must be at least 8 characters long and contain at least one uppercase letter, one lowercase letter, one digit, and one special character";
  }
  return null;
};

router.post("/", async (req: Request, res: Response) => {
  const {
    user_username,
    user_first_name,
    user_last_name,
    user_email,
    user_password,
    user_isSuperiorAdmin,
    user_isSquadAdmin,
    user_isTribeAdmin,
    user_avatar,
    user_squad_fk,
    user_role_fk,
  } = req.body;

  try {
    if (!user_email || !user_password || !user_username) {
      return res
        .status(400)
        .json({ error: "Email, password, and username are required" });
    }
    if (!user_email.includes("@") || user_email.length !== 18) {
      return res.status(400).json({ error: "Invalid email format" });
    }

    const passwordError = validatePassword(user_password);
    if (passwordError) {
      return res.status(400).json({ error: passwordError });
    }

    const existingUsersByEmail = await executeQuery(
      "SELECT * FROM users WHERE user_email = ?",
      [user_email]
    );
    if (existingUsersByEmail.length > 0) {
      return res
        .status(400)
        .json({ error: "User with this email already exists" });
    }

    const existingUsersByUsername = await executeQuery(
      "SELECT * FROM users WHERE user_username = ?",
      [user_username]
    );
    if (existingUsersByUsername.length > 0) {
      return res
        .status(400)
        .json({ error: "User with this username already exists" });
    }

    const user_id = uuidv4().replace(/-/g, "");
    const hashedPassword = await bcrypt.hash(user_password, 10);
    let tribeLeadFk = null;

    if (user_isTribeAdmin && user_squad_fk) {
      const squadResult = await executeQuery(
        "SELECT squad_tribe_fk FROM squads WHERE squad_id = ?",
        [user_squad_fk]
      );
      if (squadResult.length > 0) {
        tribeLeadFk = squadResult[0].squad_tribe_fk;
      }
    }

    await executeQuery(
      `INSERT INTO users 
    (user_id, user_created_at, user_updated_at, user_username, user_first_name, user_last_name, user_email, user_password, user_isSuperiorAdmin, user_isSquadAdmin, user_isTribeAdmin, user_avatar, user_squad_fk, user_role_fk) 
   VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)`,
      [
        user_id,
        new Date(),
        new Date(),
        user_username,
        user_first_name,
        user_last_name,
        user_email,
        hashedPassword,
        user_isSuperiorAdmin || 0,
        user_isSquadAdmin || 0,
        user_isTribeAdmin || 0,
        user_avatar || null,
        user_squad_fk,
        user_role_fk,
      ]
    );

    // Inside your API endpoint after inserting the user
    const insertedUserId = user_id;

    // Insert into tribe_leadership if user is a tribe admin and a squad is selected
    if (user_isTribeAdmin && user_squad_fk && tribeLeadFk) {
      await executeQuery(
        `INSERT INTO tribe_leadership (user_fk, tribe_fk) VALUES (?, ?)`,
        [insertedUserId, tribeLeadFk]
      );
    }

    res.status(201).json({ message: "User created successfully" });
  } catch (error) {
    console.error("Error during user creation:", error);
    res.status(500).json({ error: "Internal server error" });
  }
});

module.exports = router;
