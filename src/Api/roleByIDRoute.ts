import express, { Request, Response } from "express";
const { executeQuery } = require("../../mySQLConnect");

const router = express.Router();

// Route to fetch role name based on user_role_fk
router.get("/:roleId", async (req: Request, res: Response) => {
  const { roleId } = req.params;

  try {
    // Fetch user role from the database based on user_role_fk
    const query = "SELECT role_name FROM user_roles WHERE role_id = ?";
    const result = await executeQuery(query, [roleId]);

    if (!result || result.length === 0) {
      return res.status(404).json({ error: "Role not found" });
    }

    const roleName = result[0].role_name;
    res.json({ roleName });
  } catch (error) {
    console.error("Error fetching user role:", error);
    res.status(500).json({ error: "Internal server error" });
  }
});

module.exports = router;
