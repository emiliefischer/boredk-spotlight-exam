import express from "express";
const router = express.Router();
const { executeQuery } = require("../../mySQLConnect");

router.post("/", async (req, res) => {
  const { user_id, product_id } = req.body;

  if (!user_id || !product_id) {
    return res.status(400).json({ error: "userId or productId is missing" });
  }

  // Check om bruger allerede har liked produktet
  const checkQuery =
    "SELECT * FROM user_favorites WHERE user_id = ? AND product_id = ?";
  const insertQuery =
    "INSERT INTO user_favorites (user_id, product_id) VALUES (?, ?)";

  try {
    const [existingFavorite] = await executeQuery(checkQuery, [
      user_id,
      product_id,
    ]);

    if (existingFavorite) {
      return res
        .status(400)
        .json({ error: "User has already liked this product" });
    }

    // Hvis produktet ikke er liked, indsæt i database
    await executeQuery(insertQuery, [user_id, product_id]);
    console.log(`Product ${product_id} added to favorites for user ${user_id}`);
    res
      .status(201)
      .json({ message: "Product added to favorites successfully" });
  } catch (error) {
    console.error(
      `Error adding product ${product_id} to favorites for user ${user_id}:`,
      error
    );
    res.status(500).json({ error: "Internal server error" });
  }
});

module.exports = router;
