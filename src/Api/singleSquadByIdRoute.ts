const express = require("express");
const router = express.Router();
const { executeQuery } = require("../../mySQLConnect");
import { Response, Request } from "express";

// Get squad by its ID
router.get("/:squadId", async (req: Request, res: Response) => {
  const { squadId } = req.params;
  try {
    console.log(`Fetching squad with ID: ${squadId}`);
    const query = `SELECT * FROM squads WHERE squad_id = ?`;
    const squads = await executeQuery(query, [squadId]);
    if (squads.length > 0) {
      console.log("Squad fetched:", squads[0]);
      res.json(squads[0]);
    } else {
      res.status(404).json({ error: "Squad not found" });
    }
  } catch (error) {
    console.error("Error retrieving squad:", error);
    res.status(500).json({ error: "Internal server error" });
  }
});

module.exports = router;
