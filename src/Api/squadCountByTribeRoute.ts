const express = require("express");
const router = express.Router();
const { executeQuery } = require("../../mySQLConnect");
import { Response, Request } from "express";

// Get count of squads related to a specific tribe
router.get("/:tribeId", async (req: Request, res: Response) => {
  const { tribeId } = req.params;

  try {
    console.log(`Fetching squad count for tribe ${tribeId} from database...`);
    const query = `
      SELECT COUNT(*) AS squadCount
      FROM squads
      WHERE squad_tribe_fk = ?
    `;
    const result = await executeQuery(query, [tribeId]);
    const squadCount = result[0].squadCount;
    console.log(`Squad count for tribe ${tribeId} fetched:`, squadCount);
    res.json({ squadCount });
  } catch (error) {
    console.error(`Error retrieving squad count for tribe ${tribeId}:`, error);
    res.status(500).json({ error: "Internal server error" });
  }
});

module.exports = router;
