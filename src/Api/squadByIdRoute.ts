import express, { Request, Response } from "express";
const { executeQuery } = require("../../mySQLConnect");

const router = express.Router();

router.get("/:squadId", async (req: Request, res: Response) => {
  const { squadId } = req.params;

  try {
    const query = "SELECT squad_name FROM squads WHERE squad_id = ?";
    const result = await executeQuery(query, [squadId]);

    if (!result || result.length === 0) {
      return res.status(404).json({ error: "Squad not found" });
    }

    const squadName = result[0].squad_name;
    res.json({ squadName });
  } catch (error) {
    console.error("Error fetching squad name:", error);
    res.status(500).json({ error: "Internal server error" });
  }
});

module.exports = router;
