import { defineConfig } from "cypress";

export default defineConfig({
  projectId: "g6ytfj",
  e2e: {
    video: true,
    videoCompression: true,
    experimentalStudio: true,
    env: {
      baseUrl: "http://localhost:4000",
    },
    setupNodeEvents(on, config) {},
  },
});
