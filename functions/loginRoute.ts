import cookieParser from "cookie-parser";
import express, { Request, Response } from "express";
import jwt from "jsonwebtoken";
import bcrypt from "bcrypt";
const router = express.Router();
const { executeQuery } = require("../functions/mySQLConnect");

router.use(cookieParser());

router.post("/", async (req: Request, res: Response) => {
  const { user_email, user_password } = req.body;

  try {
    // Fetch the hashed password associated with the provided email
    const users = await executeQuery(
      "SELECT * FROM users WHERE user_email = ?",
      [user_email]
    );

    if (users.length === 1) {
      const user = users[0];
      const hashedPassword = user.user_password;

      // Compare the provided password with the hashed password from the database
      const passwordMatch = await bcrypt.compare(user_password, hashedPassword);

      if (passwordMatch) {
        // If passwords match then generate JWT token
        const tokenPayload = {
          userId: user.user_id,
          isSuperiorAdmin: user.user_isSuperiorAdmin,
          isSquadAdmin: user.user_isSquadAdmin,
          isTribeAdmin: user.user_isTribeAdmin,
        };

        const token = jwt.sign(tokenPayload, process.env.TOKEN_SECRET!, {
          expiresIn: "1d",
        });

        // Set the token in a cookie
        res.cookie("token", token, {
          httpOnly: true,
          secure: process.env.NODE_ENV === "production",
          sameSite: "strict",
          maxAge: 24 * 60 * 60 * 1000,
        });

        // Send the token and role flags in the response body
        res.status(200).json({
          message: "Login successful",
          token,
          isSuperiorAdmin: user.user_isSuperiorAdmin,
          isSquadAdmin: user.user_isSquadAdmin,
          isTribeAdmin: user.user_isTribeAdmin,
        });
      } else {
        // If passwords don't match, return error message
        res.status(401).json({ error: "Invalid email or password" });
      }
    } else {
      // If no user found with provided email, return error message
      res.status(401).json({ error: "Invalid email or password" });
    }
  } catch (error) {
    console.error("Error during login:", error);
    res.status(500).json({ error: "Internal server error" });
  }
});

module.exports = router;
