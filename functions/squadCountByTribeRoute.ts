import { builder, Handler } from "@netlify/functions";
const { executeQuery } = require("../functions/mySQLConnect");

const fetchSquadCountByTribeHandler: Handler = async (event, context) => {
  const tribeId = event.path.split("/").pop(); // Getting the tribeId from the URL path
  if (!tribeId) {
    return {
      statusCode: 400,
      body: JSON.stringify({ error: "Tribe ID is required" }),
    };
  }

  try {
    console.log(`Fetching squad count for tribe ${tribeId} from database...`);
    const query = `
      SELECT COUNT(*) AS squadCount
      FROM squads
      WHERE squad_tribe_fk = ?
    `;
    const result = await executeQuery(query, [tribeId]);
    const squadCount = result[0]?.squadCount || 0;
    console.log(`Squad count for tribe ${tribeId} fetched:`, squadCount);
    return {
      statusCode: 200,
      body: JSON.stringify({ squadCount }),
    };
  } catch (error) {
    console.error(`Error retrieving squad count for tribe ${tribeId}:`, error);
    return {
      statusCode: 500,
      body: JSON.stringify({ error: "Internal server error" }),
    };
  }
};

const handler = builder(fetchSquadCountByTribeHandler);

export { handler };
