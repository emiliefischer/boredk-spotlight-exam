import { builder, Handler } from "@netlify/functions";
const { executeQuery } = require("../functions/mySQLConnect");

const fetchSingleSquadHandler: Handler = async (event, context) => {
  const squadName = event.queryStringParameters?.squadName;
  if (!squadName) {
    return {
      statusCode: 400,
      body: JSON.stringify({ error: "Squad name is required" }),
    };
  }
  try {
    console.log(`Fetching squad with name: ${squadName}`);
    const query = `
      SELECT * FROM squads WHERE LOWER(REPLACE(squad_name, ' ', '-')) = ?
    `;
    const squads = await executeQuery(query, [
      squadName.toLowerCase().replace(/ /g, "-"),
    ]);
    if (squads.length > 0) {
      console.log("Squad fetched:", squads[0]);
      return {
        statusCode: 200,
        body: JSON.stringify(squads[0]),
      };
    } else {
      return {
        statusCode: 404,
        body: JSON.stringify({ error: "Product not found" }),
      };
    }
  } catch (error) {
    console.error("Error retrieving squad:", error);
    return {
      statusCode: 500,
      body: JSON.stringify({ error: "Internal server error" }),
    };
  }
};

const handler = builder(fetchSingleSquadHandler);

export { handler };
