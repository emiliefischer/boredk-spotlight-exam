import { builder, Handler } from "@netlify/functions";
const { executeQuery } = require("../functions/mySQLConnect");

const fetchSingleTribeHandler: Handler = async (event, context) => {
  const tribeName = event.queryStringParameters?.tribeName;
  if (!tribeName) {
    return {
      statusCode: 400,
      body: JSON.stringify({ error: "Tribe name is required" }),
    };
  }
  try {
    console.log(`Fetching tribe with name: ${tribeName}`);
    const query = `
      SELECT * FROM tribes WHERE LOWER(REPLACE(tribe_name, ' ', '-')) = ?
    `;
    const tribes = await executeQuery(query, [
      tribeName.toLowerCase().replace(/ /g, "-"),
    ]);
    if (tribes.length > 0) {
      console.log("Tribe fetched:", tribes[0]);
      return {
        statusCode: 200,
        body: JSON.stringify(tribes[0]),
      };
    } else {
      return {
        statusCode: 404,
        body: JSON.stringify({ error: "Tribe not found" }),
      };
    }
  } catch (error) {
    console.error("Error retrieving tribe:", error);
    return {
      statusCode: 500,
      body: JSON.stringify({ error: "Internal server error" }),
    };
  }
};

const handler = builder(fetchSingleTribeHandler);

export { handler };
