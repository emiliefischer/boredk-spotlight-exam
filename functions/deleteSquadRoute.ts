import express from "express";
const { executeQuery } = require("../functions/mySQLConnect");

const router = express.Router();

// Delete a squad
router.delete("/:squadId", async (req, res) => {
  try {
    const { squadId } = req.params;

    // Query to delete a squad from the database
    const query = `
      DELETE FROM squads WHERE squad_id = ?
    `;

    await executeQuery(query, [squadId]);

    res.status(200).json({ message: "Squad deleted successfully" });
  } catch (error) {
    console.error("Error deleting squad:", error);
    res.status(500).json({ error: "Internal server error" });
  }
});

module.exports = router;
